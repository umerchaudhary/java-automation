package Controller;

import Drivers.Chrome;
import Drivers.Edge;
import Drivers.Firefox;
import FileReadWrite.networkCopy;
import Reports.ExtentManager;
import Services.*;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.IHookCallBack;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import org.testng.internal.TestResult;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Listeners(Listener.class)
public class BaseController implements org.testng.IHookable{
    private WebDriver driver;
    protected volatile GlobalVariables GV;
    Chrome tempChromeHandler;

    public BaseController(){



    }


    //final private CommonFunc CF=new CommonFunc();
    //final private DataProcessor DP=new DataProcessor();
    //public ExtentTest test;


    @BeforeSuite
    public void Starter(ITestContext context) {     //Starter starts the browser on which testing is to be conducted
        //GV=new GlobalVariables();
        GV=GlobalVariablesManager.objectCreationMethod().getGlobalVariable();
        //setBrowser();

        //ExtentManager extent=new ExtentManager(GV);
        //extent.filePath=extent.filePath.replace("extentreport.html","extentreport_"+GV.recoId+"_"+context.getCurrentXmlTest().getSuite().getName().replace(" ","")+".html");
        /*
        GV.extent=extent.GetExtent();
        CF.GV=GV;
        GV.CF=CF;
        DP.GV=GV;
        */
        //GV.extent= GV.extentManager.GetExtent();
        //CommonFunc.GV =GV;
        //GV.CF=CF;
        //DataProcessor.GV =GV;
        //GV.dataProcessor=DP;
        String log4jConfPath = System.getProperty("user.dir") + "/log4j2.properties";
        PropertyConfigurator.configure(log4jConfPath);
        System.out.println(java.lang.Thread.currentThread());
        System.out.println(java.lang.Thread.activeCount());
        GV.suiteName=context.getSuite().getName();
        /*if (GlobalVariablesManager.firstSetup()) {
            System.out.println("Setting Reporting Environment");
            //GV.extentManager.reportEnvironment();
            /*try {
                    FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "\\test-output\\extentReports"));
            }
                catch (Exception e) {
                    GV.CF.logInfo(null,"Exception - Trying to clean directory, BaseController.Java|Starter");
            }
            Listener.firstRunFlag=false;
        }*/

    }

    @BeforeTest
    public void testStarter(ITestContext testContext){
        GV=GlobalVariablesManager.objectCreationMethod().getGlobalVariable();
        GV.testName=testContext.getName();

    }


    @AfterSuite
    public void Closer(){                       //kills browser after batch execution is complete
        WebDriver actualDriver=null;
        if (GV.browser.toLowerCase().contains("browserstack")){
            actualDriver=GV.driver;
            tempChromeHandler = new Chrome(0);
            GV.driver = tempChromeHandler.chromeDriver();
        }
        try{
            createEmailableImage();
            //createEmailableImageDashboard();
        }
        catch (Exception e){
            System.out.println("Failed to create Email Image");}

        if (GV.browser.toLowerCase().contains("browserstack")) {
            GV.driver.close();
            try{GV.driver.quit();}
            catch (Exception e){
                GV.CF.logInfo(null,"Exception - Trying to quit driver after closing, BaseController.Java|Closer");
            }
            GV.driver=actualDriver;
        }
        GV.driver.close();
        try{GV.driver.quit();}
        catch (Exception e){
            GV.CF.logInfo(null,"Exception - Trying to quit driver after closing, BaseController.Java|Closer");
        }
        if (GV.browser.toLowerCase().contains("browserstack")) {
            try {
                GV.bsLocal.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        GV.driver=null;
    }




    public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
        //****BEORE START OF TEST CASE*************
        /*BaseController.GV.softAssertion=new SoftAssert();
        BaseController.GV.test=GV.extent.createTest(iTestResult.getMethod().getMethodName());
        BaseController.GV.test.assignCategory(GV.testName);*/
        GV.softAssertion=new SoftAssert();
        if (GV.testSuite==null || !GV.testName.equals(GV.className)) {
            GV.testSuite = GV.extent.createTest(GV.testName);
            GV.className=GV.testName;
        }
        GV.test=GV.testSuite.createNode(iTestResult.getMethod().getMethodName());
        /*GV.test = GV.extent.createTest(iTestResult.getMethod().getMethodName());
        GV.testSuite=GV.test;*/
        GV.testTest=GV.test;
        GV.testStep=GV.test;
        GV.testSubStep=GV.test;
        GV.test.assignCategory(GV.testName);
        GV.logger= LogManager.getLogger(iTestResult.getClass());
        GV.skipFlag=false;
        GV.logger.info(GV.suiteName+" | "+"******************************************************");
        GV.logger.info(GV.suiteName+" | "+"************Starting Test Case: " + iTestResult.getMethod().getMethodName() + "************");
        GV.logger.info(GV.suiteName+" | "+"******************************************************");
        iHookCallBack.runTestMethod(iTestResult);   //****TEST CASE EXECUTING************
        //****AFTER TEST CASE*************
        if (iTestResult.getThrowable()!=null){
            if (!iTestResult.getThrowable().getCause().getClass().toString().equals("class java.lang.AssertionError"))
                GV.CF.logError(iTestResult,"");
        }
        if (GV.skipFlag){
            iTestResult.setStatus(TestResult.SKIP);
        }
        GV.extentManager.GetExtent().flush();
        GV.softAssertion.assertAll();

    }

    private void createEmailableImage(){ //Create image for Email attachment
        Action Ac=new Action(GV);
        int i=0;
        String filePath=System.getProperty("user.dir")+ "\\test-output\\extentReports\\" + "emailable-extent" + ".png";
        List<String> htmlFileContent = Arrays.asList(
                //"<!DOCTYPE html>\n",
                "<html>\n",
                "<body>\n",
                //"<img src=\""+filePath+"\" alt=\"Results\" width=\"460\" height=\"345\">\n",
                "<p><img src=\""+"emailable-extent" + ".png"+"\" alt=\"Results\" ></p>\n",
                "</body>\n",
                "</html>\n");
        Path htmlFilePath = Paths.get(System.getProperty("user.dir")+ "\\test-output\\extentReports\\" + "emailable-extent" + ".html");
        GV.driver.get("file:///"+System.getProperty("user.dir")+"\\test-output\\extentReports\\extentreport.html");
        GV.CF.delay(3000);
        try{
            List<WebElement> objs=GV.CF.findElements(By.xpath("//ul[@id='slide-out']/li"));
            Ac.click(objs.get(objs.size()-1));
            WebElement obj=GV.CF.findElement(By.id("dashboard-view"));
            while (i<5){
                if (GV.CF.findElement(By.id("dashboard-view")).getRect().width>0)
                    i=6;
                else{
                    i++;
                    GV.CF.delay(1000);
                }
            }

            File source = obj.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File(filePath));
            Files.write(htmlFilePath,htmlFileContent,Charset.forName("UTF-8"));
        }
        catch (Exception e){
            System.out.println(e.getCause().toString());
        }
    }


    private void createEmailableImageDashboard(){ //Create image for Email attachment
        Action Ac=new Action(GV);
        int i=0;
        String filePath=System.getProperty("user.dir")+ "\\test-output\\extentReports\\" + "emailable-extent" + ".png";
        List<String> htmlFileContent = Arrays.asList(
                //"<!DOCTYPE html>\n",
                "<html>\n",
                "<body>\n",
                //"<img src=\""+filePath+"\" alt=\"Results\" width=\"460\" height=\"345\">\n",
                "<p><img src=\""+"emailable-extent" + ".png"+"\" alt=\"Results\" ></p>\n",
                "</body>\n",
                "</html>\n");
        Path htmlFilePath = Paths.get(System.getProperty("user.dir")+ "\\test-output\\extentReports\\" + "emailable-extent" + ".html");
        GV.driver.get("file:///"+System.getProperty("user.dir")+"\\test-output\\extentReports\\dashboard.html");
        GV.CF.delay(3000);
        //List<WebElement> objs=GV.driver.findElements(By.xpath("//ul[@id='slide-out']/li"));
        //Ac.click(objs.get(objs.size()-1));
        WebElement obj=GV.CF.findElement(By.className("main-content"));
        while (i<5){
            if (GV.CF.findElement(By.className("main-content")).getRect().width>0)
                i=6;
            else{
                i++;
                GV.CF.delay(1000);
            }
        }
        try{
            File source = obj.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File(filePath));
            Files.write(htmlFilePath,htmlFileContent,Charset.forName("UTF-8"));
        }
        catch (Exception e){
            System.out.println(e.getCause().toString());
        }
    }

    private void setBrowser(){
        if (GV.browser.toLowerCase().equals("chrome")) {
            Chrome chromeHandler = new Chrome(0);
            driver = chromeHandler.chromeDriver();
        }
        if (GV.browser.toLowerCase().equals("firefox")) {
            Firefox firefoxHandler = new Firefox();
            driver = firefoxHandler.firefoxDriver();
        }
        if (GV.browser.toLowerCase().equals("edge")) {
            Edge edgeHandler = new Edge();
            driver = edgeHandler.edgeDriver();
        }
        if (GV.browser.toLowerCase().equals("chrome beta")) {
            Chrome chromeHandler = new Chrome(1);
            driver = chromeHandler.chromeDriver();
        }
        GV.driver=driver;
        driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);

    }

    public void networkCopy(){
        networkCopy networkCopy=new networkCopy();
        String currentDirectory="";
        String qaDirectory="swbrdskqa-ola21/d$/Temp/Users/CobrandFolders/cid-171/AutomatedTests/LatestMNRTest";
        String stageDirectory="nfprgnt-ola11.rediv.int/public/Stage/wwwroot/rdesk.com/Users/CobrandFolders/cid-171/AutomatedTests/LatestMNRTests";
        String prodDirectory="nfprgnt-ola11.rediv.int/public/wwwroot/rdesk.com/Users/CobrandFolders/cid-171/AutomatedTests/LatestMNRTests";
        switch (GV.environment){
            case "PRODUCTION":
                currentDirectory=prodDirectory;
                break;
            case "QA":
                currentDirectory=qaDirectory;
                break;
            case "STAGE":
                currentDirectory=stageDirectory;
        }

        try{
            String src=System.getProperty("user.dir") + "\\test-output\\extentReports\\";
            networkCopy.deleteFileFolder("rediv","uniaz","b3Utoetc!!##","smb://"+currentDirectory+"/");
            networkCopy.createFolderOnNetwork("rediv","uniaz","b3Utoetc!!##","smb://"+currentDirectory);
            File folder = new File(src);
            File[] listOfFiles = folder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    networkCopy.createCopyOnNetwork("rediv", "uniaz", "b3Utoetc!!##", listOfFiles[i].getPath(), "smb://"+currentDirectory+"/"+listOfFiles[i].getName());
                }
            }

        }
        catch (Exception ex){}

    }

}
