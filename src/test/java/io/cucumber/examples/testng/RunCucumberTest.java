package io.cucumber.examples.testng;

import Controller.BaseController;
import Services.GlobalVariablesManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.testng.annotations.DataProvider;
import org.testng.asserts.SoftAssert;
import org.testng.internal.TestResult;

@CucumberOptions(plugin = { "html:target/results.html", "message:target/results.ndjson" })
public class RunCucumberTest extends AbstractTestNGCucumberTests {

    @DataProvider(parallel = false)
    @Override
    public Object[][] scenarios() {
        return super.scenarios();
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void flush() {
    }

}
