package io.cucumber.examples.testng;

import Controller.BaseController;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

/**
 * An example base class
 */
class RunCucumberByCompositionBase extends BaseController {

    @BeforeClass
    public void beforeClass() {
        // do expensive setup
    }

    @BeforeMethod
    public void beforeMethod() {
        // do expensive setup
    }

}
