package io.cucumber.examples.testng;

import Services.Action;
import Services.GlobalVariables;
import Services.GlobalVariablesManager;
import Services.Verification;
import org.apache.log4j.PropertyConfigurator;
import org.apache.logging.log4j.LogManager;
import org.testng.asserts.SoftAssert;
import org.testng.internal.TestResult;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

public class RpnCalculator {

    private static final List<String> OPS = asList("-", "+", "*", "/");
    private final Deque<Number> stack = new LinkedList<>();
    private GlobalVariables GV;
    private final Action Ac;
    private final Verification Ver;

    public RpnCalculator(GlobalVariables gv) {
        GV=GlobalVariablesManager.getGlobalVariable();
        Ac = new Action(GV);
        Ver = new Verification(GV);

    }

    public void push(Object arg) {
        if (OPS.contains(arg)) {
            Number y = stack.removeLast();
            Number x = stack.isEmpty() ? 0 : stack.removeLast();
            Double val = null;
            String operator=null;
            if (arg.equals("-")) {
                val = x.doubleValue() - y.doubleValue();
            } else if (arg.equals("+")) {
                val = x.doubleValue() + y.doubleValue();
            } else if (arg.equals("*")) {
                val = x.doubleValue() * y.doubleValue();
            } else if (arg.equals("/")) {
                val = x.doubleValue() / y.doubleValue();
            }
            push(val);
            GV.CF.logPass(null,x + " " + arg.toString() + " " + y+ " = " + val);
        } else {
            stack.add((Number) arg);
        }
    }

    public void PI() {
        push(Math.PI);
    }

    public Number value() {
        return stack.getLast();
    }

}
