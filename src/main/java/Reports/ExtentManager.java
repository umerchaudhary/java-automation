package Reports;


import Services.GlobalVariables;
import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import com.aventstack.extentreports.reporter.ExtentLoggerReporter;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ExtentHtmlReporterConfiguration;
import com.aventstack.extentreports.reporter.configuration.ExtentSparkReporterConfiguration;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.apache.commons.exec.OS;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;

public class ExtentManager {
    private GlobalVariables GV;
    private String fileName;
    private ExtentReports extent;
    private ExtentTest test;
    private ExtentHtmlReporter htmlReporter;
    //private ExtentLoggerReporter loggerReporter;
    private ExtentSparkReporter sparkReporter;
    private static final String filePath = "./test-output/extentReports/extentreport.html";
    private static final String newFilePath = "./test-output/extentReports/newReport.html";

    public int count=0;
    private int flushCount=0;

    public ExtentManager() {

        //GV=GVGlobal;

    }

    public void setGV(GlobalVariables GV2){
        GV=GV2;
    }




    public ExtentReports GetExtent(){
        if (extent != null)
            return extent; //avoid creating new instance of html file
        extent = new ExtentReports();
        extent.attachReporter(getHtmlReporter());
        //extent.attachReporter(getLoggerReporter());
        extent.attachReporter(getSparkReporter());
        //extent.setAnalysisStrategy(AnalysisStrategy.SUITE);
        return extent;
    }

    private ExtentHtmlReporter getHtmlReporter() {
        //filePath=filePath.replace("extentreport.html","extentreport_"+reportName+".html");
        //flushReportingDirectory();
        htmlReporter = new ExtentHtmlReporter(filePath);

        // make the charts visible on report open
        //htmlReporter.config().setChartVisibilityOnOpen(true);

        htmlReporter.config().setDocumentTitle("RED automation report");
        htmlReporter.config().setReportName("Regression cycle");
        //htmlReporter.config().enableTimeline(true);
        htmlReporter.config().setProtocol(Protocol.HTTP);
        //htmlReporter.config().setTheme(Theme.DARK);
        //htmlReporter.setAppendExisting(true);
        return htmlReporter;
    }

    /*
    private ExtentLoggerReporter getLoggerReporter() {
        //filePath=filePath.replace("extentreport.html","extentreport_"+reportName+".html");
        //flushReportingDirectory();
        loggerReporter = new ExtentLoggerReporter(filePath);

        // make the charts visible on report open
        //htmlReporter.config().setChartVisibilityOnOpen(true);

        loggerReporter.config().setDocumentTitle("RED automation report");
        loggerReporter.config().setReportName("Regression cycle");
        loggerReporter.config().enableTimeline(true);
        loggerReporter.config().setProtocol(Protocol.HTTP);
        loggerReporter.config().setTheme(Theme.DARK);
        //htmlReporter.setAppendExisting(true);
        return loggerReporter;
    }*/

    private ExtentSparkReporter getSparkReporter() {
        //filePath=filePath.replace("extentreport.html","extentreport_"+reportName+".html");
        //flushReportingDirectory();
        sparkReporter = new ExtentSparkReporter(newFilePath);

        // make the charts visible on report open
        //htmlReporter.config().setChartVisibilityOnOpen(true);

        sparkReporter.config().setDocumentTitle("RED automation report");
        sparkReporter.config().setReportName("Regression cycle");
        sparkReporter.config().enableTimeline(true);
        sparkReporter.config().setProtocol(Protocol.HTTP);
        sparkReporter.config().setTheme(Theme.DARK);
        //htmlReporter.setAppendExisting(true);
        return sparkReporter;
    }

    public ExtentTest createTest(String name, String description){
        test = extent.createTest(name, description);
        return test;
    }

    public void reportEnvironment(GlobalVariables GV2){
        if (count==0) {
            count++;
            /*System.out.println("Cleaning Directory");
            try {
                FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "\\test-output\\extentReports"));
                if (new File(System.getProperty("user.dir") + "\\test-output\\extentReports\\extentreport.html").exists())
                    FileUtils.forceDelete(new File(System.getProperty("user.dir") + "\\test-output\\extentReports\\extentreport.html"));
            } catch (Exception e) {
                GV.CF.logInfo(null, "Exception - Trying to clean directory, BaseController.Java|Starter");
            }*/
            Capabilities caps = ((RemoteWebDriver) GV2.driver).getCapabilities();

            GV2.extent.setSystemInfo("Login User:",System.getProperty("user.name"));
            try {
                java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
                GV2.extent.setSystemInfo("Machine Name: ",localMachine.getHostName());

            }
            catch (Exception e){
                System.out.println("Exception - Trying to get IP Address of Machine, ExtentManager.Java|reportEnvironment");
            }

            GV2.extent.setSystemInfo("Browser Name:",caps.getBrowserName());
            GV2.extent.setSystemInfo("Browser Version:",caps.getVersion());
            GV2.extent.setSystemInfo("Operating System:",System.getProperty("os.name"));
            GV2.extent.setSystemInfo("Real Estate Company:",GV2.recoId);
            GV2.extent.setSystemInfo("Environment: ",GV2.environment);
            GV2.extent.setSystemInfo("Headless Mode: ",GV2.jenkinsFlag.toString());
            GV2.extent.setSystemInfo("Data Refresh: ",GV2.getLatestDataFromDB.toString());
            GV2.extent.setSystemInfo("Platform: ",GV2.platform);

        }


    }
    void flushReportingDirectory() {

        if (flushCount == 0) {
            System.out.println("Cleaning Directory");
            try {
                FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "\\test-output\\extentReports"));
                if (new File(System.getProperty("user.dir") + "\\test-output\\extentReports\\extentreport.html").exists())
                    FileUtils.forceDelete(new File(System.getProperty("user.dir") + "\\test-output\\extentReports\\extentreport.html"));
            } catch (Exception e) {
                GV.CF.logInfo(null, "Exception - Trying to clean directory, BaseController.Java|Starter");
            }
            flushCount++;
        }
    }

}