package Drivers;

import Services.GlobalVariables;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import com.browserstack.local.Local;


public class BrowserStack {
    private WebDriver driver;
    private Local bsLocal;

    public  BrowserStack(String browserName,int flag,String platform){ //flag=0 Stable Release, flag=1 beta release, flag=2 Dev release

        //if (browserName.toLowerCase().equals("browserstack chrome")) {

            bsLocal = new Local();
            HashMap<String, String> bsLocalArgs = new HashMap<String, String>();
            bsLocalArgs.put("key", "X26uqix5GP8jHzxpqxht");
            bsLocalArgs.put("acceptSslCerts", "true");
            bsLocalArgs.put("forcelocal", "true");

            try{
                bsLocal.start(bsLocalArgs);
                System.out.println("Browserstack connection: "+bsLocal.isRunning());

            }
            catch (Exception ex){
                System.out.println(ex.toString());
            }


            String username = System.getenv("BROWSERSTACK_USERNAME");
            String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
            String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
            String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");

            DesiredCapabilities capabilities = new DesiredCapabilities();
            DesiredCapabilities caps = new DesiredCapabilities();
            String platformArray[]=platform.split("_");
            String browserArray[]=browserName.split("_");
            caps.setCapability("os", platformArray[0]);
            caps.setCapability("os_version", platformArray[1]);
            caps.setCapability("browser", browserArray[1]);
            caps.setCapability("browser_version", browserArray[2]);
            //caps.setCapability("project", "PROJECT_NAME");
            //caps.setCapability("build", "BUILD_NAME");
            //caps.setCapability("name", "SESSION_NAME");
            caps.setCapability("browserstack.local", "false");
            caps.setCapability("browserstack.debug", "true");
            caps.setCapability("browserstack.console", "info");
            caps.setCapability("browserstack.networkLogs", "true");
            //caps.setCapability("browserstack.chrome.driver", "2.45");
            //caps.setCapability("browserstack.user", "USERNAME");
            //caps.setCapability("browserstack.key", "ACCESS_KEY");


            //capabilities.setCapability("browserstack.local", bsLocalArgs);
            //capabilities.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
            try {
                username="umerchaudhary1";
                accessKey="X26uqix5GP8jHzxpqxht";
                driver = new RemoteWebDriver(new URL("https://" + username + ":" + accessKey + "@hub.browserstack.com/wd/hub"), caps);
                ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        //}

    }
    public WebDriver chromeDriver(){
        return this.driver;
    }
    public Local browserStackLocal(){
        return this.bsLocal;
    }

}
