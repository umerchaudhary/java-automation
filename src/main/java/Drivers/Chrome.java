package Drivers;

import Services.GlobalVariables;
import com.sun.prism.image.ViewPort;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.remote.CapabilityType.PLATFORM_NAME;


public class Chrome {
    private WebDriver driver;

    public Chrome(int flag){ //flag=0 Stable Release, flag=1 beta release, flag=2 Dev release
        WebDriverManager.chromedriver().setup();
        //WebDriverManager.getInstance(CHROME).setup();
        //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\ExtUtils\\chromedriver\\chromedriver.exe");
        String pathToExtension = "C:\\Users\\uniaz\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\ajhmfdgkijocedmfjonnpjfojldioehi\\2.0.6_0";
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("–load-extension=" + pathToExtension);
        //chromeOptions.addArguments("user-data-dir=C:\\Users\\uniaz\\AppData\\Local\\Google\\Chrome\\User Data_auto");
        chromeOptions.addArguments("disable-infobars");
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        if (flag==1)
            chromeOptions.setBinary("C:\\Program Files (x86)\\Google\\Chrome Beta\\Application\\chrome.exe");
        if (GlobalVariables.jenkinsFlag) {
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("window-size=1920,1080");
            chromeOptions.addArguments("--disable-gpu");
        }
        if (!GlobalVariables.platform.toLowerCase().contains("windows")){
            Map<String, String> mobileEmulation = new HashMap<>();
            mobileEmulation.put("deviceName", GlobalVariables.platform);
            chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        }
        chromeOptions.addArguments("start-maximized");
        driver=new ChromeDriver(chromeOptions);
        driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
    }
    public WebDriver chromeDriver(){
        return this.driver;
    }

}
