package Drivers;



import Services.GlobalVariables;
import com.google.common.base.Stopwatch;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;


public class DataAccess {

    private final String QA_WEB01="jdbc:sqlserver://QA;database=DB";



    private final String PROD_WEB01="jdbc:sqlserver://PROD:52938;database=DB";




    private String resultS[][];
    public enum Environment{
        STG_WEB01,STG_WEB02,STG_Tracking01,STG_Tracking02,STG_rDESK,STG_US,STG_ListingWarehouse,STG_ListingSearchActivity,STAGE_DATAVAULT,
        DEV_WEB01,DEV_WEB02,DEV_Tracking01,DEV_Tracking02,DEV_ListingWarehouse,DEV_ListingSearchActivity,DEV_DATAVAULT,DEV_rDESK,
        QA_WEB01,QA_WEB02,QA_rDESK,QA_Tracking01,QA_Tracking02,
        PROD_WEB01,PROD_WEB02,PROD_Tracking01,PROD_Tracking02,PROD_rDESK,PROD_ListingWarehouse,PROD_ListingSearchActivity,PROD_US,PROD_DATAVAULT,
        HOTFIX_WEB01,HOTFIX_WEB02,HOTFIX_Tracking01,HOTFIX_Tracking02,HOTFIX_rDESK,HOTFIX_ListingWarehouse,HOTFIX_ListingSearchActivity,HOTFIX_US,HOTFIX_DATAVAULT
    }

    public String[][] executeQuery(String query, Environment Ev){

        // Object of Connection from the Database
        Connection conn = null;

        // Object of Statement. It is used to create a Statement to execute the query
        Statement stmt = null;

        //Object of ResultSet => 'It maintains a cursor that points to the current row in the result set'
        ResultSet resultSet = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            // Open a connection
            switch (Ev) {

                case QA_WEB01:
                    conn = DriverManager.getConnection(QA_WEB01, "BODev", "fastD3v");
                    break;


                case PROD_WEB01:
                    conn = DriverManager.getConnection(PROD_WEB01, "uniaz", "7umer$4QA");
                    break;

                default:
                    System.out.println("FAILED TO IDENTIFY DB");
                    break;
            }

            // Execute a query
            Stopwatch stopwatch = Stopwatch.createStarted();
            if (conn != null) {
                stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                stmt.setQueryTimeout(180);
                resultSet = stmt.executeQuery(query);
                stopwatch.stop();
                long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
                GlobalVariables.logger.info("Query Execution Took: " + stopwatch);
                resultSet.last();
                resultS = new String[resultSet.getRow()][resultSet.getMetaData().getColumnCount()];
                resultSet.beforeFirst();
                int i, j;
                i = j = 0;
                while (resultSet.next()) {
                    for (j = 0; j < resultS[0].length; j++) {
                        resultS[i][j] = resultSet.getString(j + 1);
                    }
                    i++;

                }
            }

        }
        catch (Exception e){
            GlobalVariables.logger.error(e.getMessage());

        }
        /*if (stmt != null) {
            try {
                //stmt.close();
            } catch (Exception e) {
                GlobalVariables.logger.error(e.getMessage());
            }
        }*/

        if (conn != null) {
            try {
                conn.close();
            } catch (Exception e) {
                GlobalVariables.logger.error(e.getMessage());
            }
        }
        return resultS;

    }

}