package Drivers;

import Services.GlobalVariables;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


public class Firefox {
    private final WebDriver driver;

    public Firefox(){
        WebDriverManager.firefoxdriver().setup();
        //WebDriverManager.getInstance(CHROME).setup();
        //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\ExtUtils\\chromedriver\\chromedriver.exe");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("disable-infobars");
        if (GlobalVariables.jenkinsFlag) {
            firefoxOptions.addArguments("--headless");
            firefoxOptions.addArguments("window-size=1920,1080");
            firefoxOptions.addArguments("--disable-gpu");
        }
        driver=new FirefoxDriver(firefoxOptions);
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    public WebDriver firefoxDriver(){
        return this.driver;
    }

}
