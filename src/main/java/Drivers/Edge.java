package Drivers;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.util.concurrent.TimeUnit;


public class Edge {
    private final WebDriver driver;

    public Edge(){
        WebDriverManager.edgedriver().setup();
        EdgeOptions edgeOptions = new EdgeOptions();
        //edgeOptions.addArguments("disable-infobars");
        /*if (GlobalVariables.jenkinsFlag) {
            edgeOptions.addArguments("--headless");
            edgeOptions.addArguments("window-size=1920,1080");
            edgeOptions.addArguments("--disable-gpu");
        }*/
        driver=new EdgeDriver(edgeOptions);
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    public WebDriver edgeDriver(){
        return this.driver;
    }

}
