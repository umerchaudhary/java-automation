package FileReadWrite;

import Services.GlobalVariables;
import com.google.common.base.Stopwatch;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


public class FileRW {


        public String readFile(String path) {
            //String directory = System.getProperty("user.home");
            String result="";

// read the content from file
            try(FileReader fileReader = new FileReader(path)) {
                int ch = fileReader.read();
                while(ch != -1) {
                    //System.out.print((char)ch);
                    result=result+(char)ch;
                    ch = fileReader.read();
                }
            } catch (FileNotFoundException e) {
                // exception handling
            } catch (IOException e) {
                // exception handling
            }
            return result;
        }

    public void writeFile(String fileData, String filePath) {

// write the content in file
        try(FileWriter fileWriter = new FileWriter(filePath)) {
            fileWriter.write(fileData);
        } catch (IOException e) {
            // exception handling
        }

    }

}
