package FileReadWrite;



import jcifs.CIFSContext;
import jcifs.Configuration;
import jcifs.config.PropertyConfiguration;
import jcifs.context.BaseContext;
import jcifs.context.SingletonContext;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.NtlmPasswordAuthenticator;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;

public class networkCopy {

    public static boolean createCopyOnNetwork(String domain,String username,String password,String src, String dest) throws Exception
    {
        //FileInputStream in = null;
        SmbFileOutputStream out = null;
        BufferedInputStream inBuf = null;
        try{
            //jcifs.Config.setProperty("jcifs.smb.client.disablePlainTextPasswords","true");
            //SingletonContext.getInstance().withCredentials(ntlmPasswordAuthentication);
            //NtlmPasswordAuthenticator authentication = new NtlmPasswordAuthenticator(domain, username, password);
            //NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication(domain,username,password); // replace with actual values
            //SmbFile file = new SmbFile(dest, authentication); // note the different format

            BaseContext baseCxt = null;
            Properties jcifsProperties  = new Properties();
            Configuration config = new PropertyConfiguration(jcifsProperties);
            baseCxt = new BaseContext(config);
            CIFSContext auth = baseCxt.withCredentials(new NtlmPasswordAuthenticator(domain, username,
                    password));
            SmbFile file = new SmbFile(dest, auth);


            //in = new FileInputStream(src);
            inBuf = new BufferedInputStream(new FileInputStream(src));
            out = (SmbFileOutputStream)file.getOutputStream();
            byte[] buf = new byte[5242880];
            int len;
            while ((len = inBuf.read(buf)) > 0){
                out.write(buf, 0, len);
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
        finally{
            try{
                if(inBuf!=null)
                    inBuf.close();
                if(out!=null)
                    out.close();
            }
            catch(Exception ex)
            {}
        }
        System.out.print("\n File copied to destination");
        return true;
    }

    public static boolean createFolderOnNetwork(String domain,String username,String password,String dest) throws Exception
    {
        //FileInputStream in = null;
        SmbFileOutputStream out = null;
        BufferedInputStream inBuf = null;
        try{
            //jcifs.Config.setProperty("jcifs.smb.client.disablePlainTextPasswords","true");
            //NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication(domain,username,password); // replace with actual values
            //SmbFile file = new SmbFile(dest, authentication); // note the different format

            BaseContext baseCxt = null;
            Properties jcifsProperties  = new Properties();
            Configuration config = new PropertyConfiguration(jcifsProperties);
            baseCxt = new BaseContext(config);
            CIFSContext auth = baseCxt.withCredentials(new NtlmPasswordAuthenticator(domain, username,
                    password));
            SmbFile file = new SmbFile(dest, auth);

            file.mkdir();
            //in = new FileInputStream(src);

        }
        catch(Exception ex)
        {
            throw ex;
        }
        System.out.print("\n Folder created to destination");
        return true;
    }

    public static boolean deleteFileFolder(String domain,String username,String password,String src) throws Exception
    {
        //FileInputStream in = null;
        SmbFileOutputStream out = null;
        BufferedInputStream inBuf = null;
        try{
            //jcifs.Config.setProperty("jcifs.smb.client.disablePlainTextPasswords","true");
            //NtlmPasswordAuthentication authentication = new NtlmPasswordAuthentication(domain,username,password); // replace with actual values
            //SmbFile file = new SmbFile(src, authentication); // note the different format

            BaseContext baseCxt = null;
            Properties jcifsProperties  = new Properties();
            Configuration config = new PropertyConfiguration(jcifsProperties);
            baseCxt = new BaseContext(config);
            CIFSContext auth = baseCxt.withCredentials(new NtlmPasswordAuthenticator(domain, username,
                    password));
            SmbFile file = new SmbFile(src, auth);

            //in = new FileInputStream(src);
            file.delete();
        }
        catch(Exception ex)
        {
            //throw ex;
        }
        System.out.print("\n File deleted");
        return true;
    }


}
