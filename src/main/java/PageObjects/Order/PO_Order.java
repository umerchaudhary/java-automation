package PageObjects.Order;

import Services.Action;
import Services.GlobalVariables;
import Services.Verification;
import org.openqa.selenium.By;

public class PO_Order {

    //************************************
    //**********GLOBAL OBJECTS*************
    //************************************
    private GlobalVariables GV;
    private final Action Ac;
    private final Verification Ver;


    //************************************
    //**********PAGE OBJECTS*************
    //************************************
    private final By cartDescription= By.xpath("//td[@class='cart_description']");



    //************************************
    //**********OBJECT FUNCTIONS*************
    //************************************
    public PO_Order(GlobalVariables GV2){
        GV=GV2;
        Ac=new Action(GV);
        Ver=new Verification(GV);

    }


    public void verifyCartDescription(String data){
        Ver.verifyObjText(GV.driver.findElement(cartDescription),"Cart Description",data);

    }


    public void verifyTitle(String data){
        Ver.Title(data);
    }


}
