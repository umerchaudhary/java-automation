package PageObjects.Index;

import Services.Action;
import Services.GlobalVariables;
import Services.Verification;
import org.openqa.selenium.By;

public class PO_Index {

    //************************************
    //**********GLOBAL OBJECTS*************
    //************************************
    private GlobalVariables GV;
    private final Action Ac;
    private final Verification Ver;


    //************************************
    //**********PAGE OBJECTS*************
    //************************************
    private final By username= By.xpath("//a[@class='account']");
    private final By signoutBtn= By.xpath("//a[@class='logout']");
    private final By signInBtn= By.xpath("//a[@class='login']");
    private final By tshirts= By.xpath("//a[@title='T-shirts']");
    private final By proceedToCheckout= By.xpath("//a[@title='Proceed to checkout']");
    private final By forgotPass= By.xpath("//a[@title='Recover your forgotten password']");
    private final By productContainer= By.xpath("//div[@class='product-container']");
    private final By addToCart= By.xpath("//a[contains(@class,'ajax_add_to_cart_button')]");
    private final By email=By.xpath("//input[@id='email']");
    private final By retrievePass=By.xpath("//form[@id='form_forgotpassword']//button[@type='submit']");
    private final By message=By.xpath("//*[contains(@class,'alert')]");




    //************************************
    //**********OBJECT FUNCTIONS*************
    //************************************
    public PO_Index(GlobalVariables GV2){
        GV=GV2;
        Ac=new Action(GV);
        Ver=new Verification(GV);

    }


    public void verifyUserName(String userName){
        Ver.verifyObjText(GV.driver.findElement(username),"User Display Name",userName);
    }

    public void verifySignOutButton(){
        Ver.verifyExists(signoutBtn,true);
    }

    public void clickSignInBtn(){
        Ac.click(GV.driver.findElement(signInBtn));
    }

    public void clickSignOutBtn(){
        Ac.click(GV.driver.findElement(signoutBtn));
    }

    public void clickTshirt(){
        Ac.click(GV.driver.findElement(tshirts));
    }

    public void hoverProduct(){
        Ac.mouseHover(GV.driver.findElement(productContainer));
    }

    public void clickAddToCart(){
        Ac.click(GV.driver.findElement(addToCart));
    }

    public void setProceedToCheckout(){
        Ac.click(GV.driver.findElement(proceedToCheckout));
    }

    public void setForgotPassword(){
        Ac.click(GV.driver.findElement(forgotPass));
    }

    public void setEmail(String emailAdd){
        Ac.setTextBoxValue(GV.driver.findElement(email),emailAdd);
    }

    public void setRetrievePassword(){
        Ac.click(GV.driver.findElement(retrievePass));
    }

    public void verifyMessage(String custMessage){
        Ver.verifyObjText(GV.driver.findElement(message),"Display Message",custMessage);
    }

    public void verifyTitle(String data){
        Ver.Title(data);
    }


}
