package PageObjects.Login;

import Services.Action;
import Services.GlobalVariables;
import Services.Verification;
import org.openqa.selenium.By;

public class PO_Login {

    //************************************
    //**********GLOBAL OBJECTS*************
    //************************************
    private GlobalVariables GV;
    private final Action Ac;
    private final Verification Ver;


    //************************************
    //**********PAGE OBJECTS*************
    //************************************
    private final By username= By.xpath("//input[@id='email']");
    private final By password= By.xpath("//input[@id='passwd']");
    private final By loginBtn= By.xpath("//button[@id='SubmitLogin']");



    //************************************
    //**********OBJECT FUNCTIONS*************
    //************************************
    public PO_Login(GlobalVariables GV2){
        GV=GV2;
        Ac=new Action(GV);
        Ver=new Verification(GV);

    }


    public void signin(String user, String pass){
        if (GV.driver.getCurrentUrl().contains("force.com") || GV.driver.getCurrentUrl().contains("")){
        }
        else {
            Ac.setTextBoxValue(GV.driver.findElement(username), user);
            Ac.setTextBoxValue(GV.driver.findElement(password), pass);
            Ac.click(GV.driver.findElement(loginBtn));
        }
        Ac.loadChecker();
    }

    public void setPassword(String data){
        Ac.setTextBoxValue(GV.driver.findElement(password),data);

    }
    public String getPassword(){
        return Ac.getText(GV.driver.findElement(password));

    }
    public void setUsername(String data){
        String name="Username";
        String actData=GV.dataProcessor.getData("userName",data);
        Ac.setTextBoxValue(GV.driver.findElement(username),actData);
    }

    public String getUsername(){
        return Ac.getText(GV.driver.findElement(username));

    }
    public void clickLogin(){

        Ac.click(GV.driver.findElement(loginBtn));
    }

    public void verifyTitle(String data){
        Ver.Title(data);
    }


}
