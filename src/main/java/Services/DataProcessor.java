package Services;


public class DataProcessor {
    private GlobalVariables GV;


    public DataProcessor(GlobalVariables GV2){
        GV=GV2;
    }

    //************************************************************
    //**************GET OBJECT NAME FOR REPORTING****************
    //************************************************************

public String getData(String elementName,String data){
        if (data.equals("!EXTERNALXL")){
            return GV.resultSetExcel.get(elementName);
        }
        else{
            return data;
        }
}

    public String getDataByKey(String Key){
        return GV.resultSetExcel.get(Key);
    }


}
