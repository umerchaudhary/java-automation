package Services;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {


    public void writeExcel(String filePath,String fileName,String sheetName,String[][] dataToWrite,Boolean reset){
        Stopwatch stopwatch = Stopwatch.createStarted();

        //Create an object of File class to open xlsx file

        File file =    new File(filePath+"\\"+fileName);

        //Create an object of FileInputStream class to read excel file.
        FileInputStream inputStream=null;

        try{inputStream = new FileInputStream(file);}
        catch (Exception e){
            System.out.println("Exception - Trying to open reading of Excel File, ExcelWriter.Java|writeExcel");
        }

        Workbook Workbook = null;

        //Find the file extension by splitting  file name in substring and getting only extension name

        String fileExtensionName = fileName.substring(fileName.indexOf("."));

        //Check condition if the file is xlsx file

        if(fileExtensionName.equals(".xlsx")){

            //If it is xlsx file then create object of XSSFWorkbook class

            try{
                if (inputStream!=null)
                    Workbook = new XSSFWorkbook(inputStream);}
            catch (Exception e)
            {
                System.out.println("Exception - Trying to open reading of Excel Workbook, ExcelWriter.Java|writeExcel");
            }

        }

        //Check condition if the file is xls file

        else if(fileExtensionName.equals(".xls")){

            //If it is xls file then create object of XSSFWorkbook class

            try{
                if (inputStream!=null)
                    Workbook = new HSSFWorkbook(inputStream);}
            catch (Exception e)
            {
                System.out.println("Exception - Trying to open reading of Excel Workbook 2, ExcelWriter.Java|writeExcel");
            }

        }

        //Read excel sheet by sheet name

        Sheet sheet=null;
        if (Workbook!=null)
            sheet = Workbook.getSheet(sheetName);

        //Get the current count of rows in excel file

        int rowCount=-1;
        if (sheet!=null)
            rowCount= sheet.getLastRowNum()-sheet.getFirstRowNum()+1;
        if (reset){
            for (int k=0;k<rowCount;k++) {
                if (sheet.getRow(k)!=null) {
                    sheet.removeRow(sheet.getRow(k));
                }
            }
            rowCount=0;
        }

        //Get the first row from the sheet

        //Row row = sheet.getRow(0);

        //Create a new row and append it at last of sheet

        //Row newRow = sheet.createRow(rowCount+1);

        //Create a loop over the cell of newly created Row


        for (int i = rowCount; i < rowCount+dataToWrite.length; i++) {

            //Row row = sheet.getRow(i).createCell().setCellValue();
            if (sheet!=null) {
                sheet.createRow(i);
                for (int j = 0; j < dataToWrite[i - rowCount].length; j++) {
                    sheet.getRow(i).createCell(j).setCellValue(dataToWrite[i - rowCount][j]);
                }
            }
        }

        //for(int j = 0; j < row.getLastCellNum(); j++){

            //Fill data in row

            //Cell cell = newRow.createCell(0);

            //cell.setCellValue(dataToWrite[0]);

        //}

        //Close input stream

        try{
            if (inputStream!=null)
                inputStream.close();
        }
        catch (Exception e)
        {
            System.out.println("Exception - Trying to open reading of Excel File 2, ExcelWriter.Java|writeExcel");
        }

        //Create an object of FileOutputStream class to create write data in excel file
        FileOutputStream outputStream=null;
        try{outputStream = new FileOutputStream(file);}
        catch (Exception e)
        {
            System.out.println("Exception - Trying to open writing of Excel File 2, ExcelWriter.Java|writeExcel");
        }

        //write data in the excel file

        try{
            if (outputStream!=null && Workbook!=null)
                Workbook.write(outputStream);
        }
        catch (Exception e)
        {
            System.out.println("Exception - Trying to open reading of Excel Workbook 3, ExcelWriter.Java|writeExcel");
        }

        //close output stream

        try{
            if (outputStream!=null)
                outputStream.close();
        }
        catch (Exception e)
        {
            System.out.println("Exception - Trying to open reading of Excel Workbook 4, ExcelWriter.Java|writeExcel");
        }
        long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
        GlobalVariables.logger.info("Excel Writing Took: " + stopwatch);

    }

    public static void main(String...strings){

        //Create an array with the data in the same order in which you expect to be filled in excel file

        String[] valueToWrite = {"Mr. E","Noida"};

        //Create an object of current class

        ExcelWriter objExcelFile = new ExcelWriter();

        //Write the file using file name, sheet name and the data to be filled

        //objExcelFile.writeExcel(System.getProperty("user.dir")+"\\src\\Data","ExportExcel.xlsx","Sheet1",valueToWrite);

    }

}
