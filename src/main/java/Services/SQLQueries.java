package Services;

import Drivers.DataAccess;
import java.util.UUID;

public class SQLQueries {

    public String[][] getLARListingInfo(String data){

        String query="select * from whatever";
        DataAccess DB=new DataAccess();
        GlobalVariables.logger.info("Executing Query *getLARListingInfo*");
        String resultSet[][]= DB.executeQuery(query,GlobalVariables.WEB_DB);
        GlobalVariables.logger.info("getLARListingInfo returned following: ");
        for (int j=0;j<resultSet.length;j++)
            System.out.println(resultSet[j][0] + resultSet[j][1] + resultSet[j][2]);
        return resultSet;
    }


}
