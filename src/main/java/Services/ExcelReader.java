package Services;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import com.google.common.base.Stopwatch;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelReader {


        public HashMap<String, String> readExcel_Office(String filePath, String fileName, String sheetName, String recoId) {
            Stopwatch stopwatch = Stopwatch.createStarted();

            //Create an object of File class to open xlsx file

            File file =    new File(filePath+"\\"+fileName);

            //Create an object of FileInputStream class to read excel file

            FileInputStream inputStream=null;
            try{inputStream = new FileInputStream(file);}
            catch (Exception e){
                System.out.println("Exception - Trying to open reading of Excel File, ExcelReader.Java|readExcel_Office");
            }

            Workbook testWorkbook = null;

            //Find the file extension by splitting file name in substring  and getting only extension name

            String fileExtensionName = fileName.substring(fileName.indexOf("."));

            //Check condition if the file is xlsx file

            if(fileExtensionName.equals(".xlsx")){

                //If it is xlsx file then create object of XSSFWorkbook class

                try{
                    if (inputStream!=null)
                        testWorkbook = new XSSFWorkbook(inputStream);}
                catch (Exception e){
                    System.out.println("Exception - Trying to open reading of WorkBook Tab, ExcelReader.Java|readExcel_Office");
                }

            }

            //Check condition if the file is xls file

            else if(fileExtensionName.equals(".xls")){

                //If it is xls file then create object of XSSFWorkbook class

                try{
                    if (inputStream!=null)
                        testWorkbook = new HSSFWorkbook(inputStream);}
                catch (Exception e){
                    System.out.println("Exception - Trying to open reading of Excel File 2, ExcelReader.Java|readExcel_Office");
                }

            }

            //Read sheet inside the workbook by its name
            Sheet testSheet=null;
            if (testWorkbook!=null) {
                testSheet = testWorkbook.getSheet(sheetName);
            }

            //Find number of rows in excel file

            if (testSheet!=null) {
                int rowCount = testSheet.getLastRowNum() - testSheet.getFirstRowNum();
                int recoColId = -1;
                Row headerRow = testSheet.getRow(0);
                for (int j = 0; j < headerRow.getLastCellNum(); j++) {
                    if (headerRow.getCell(j).toString().equals(recoId))
                        recoColId = j;
                }
                HashMap<String, String> excelData = new HashMap<String, String>();


                //Create a loop over all the rows of excel file to read it

                for (int i = 0; i < rowCount + 1; i++) {

                    Row row = testSheet.getRow(i);

                    if (row != null) {

                        if (row.getCell(0) != null && row.getCell(recoColId) != null) {
                            //System.out.println(row.getCell(0).toString() + "|| " + row.getCell(recoColId).toString());
                            excelData.put(row.getCell(0).toString(), row.getCell(recoColId).toString());
                        }
                        if (row.getCell(0) != null && row.getCell(recoColId) == null) {
                            //System.out.println(row.getCell(0).toString() + "|| " + "");
                            excelData.put(row.getCell(0).toString(), "");
                        }
                    }

                }

                stopwatch.stop();
                long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
                GlobalVariables.logger.info("Excel Reading Took: " + stopwatch);
                return excelData;
            }
            else {
                long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
                GlobalVariables.logger.info("Excel Reading Took: " + stopwatch);
                return null;
            }



        }

    public String[][] readExcel(String filePath, String fileName, String sheetName) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        String[][] resultSet;
        //Create an object of File class to open xlsx file

        File file =    new File(filePath+"\\"+fileName);

        //Create an object of FileInputStream class to read excel file

        FileInputStream inputStream=null;
        try{inputStream = new FileInputStream(file);}
        catch (Exception e){
            System.out.println("Exception - Trying to open reading of Excel File 3, ExcelReader.Java|readExcel_Office");
        }

        Workbook testWorkbook = null;

        //Find the file extension by splitting file name in substring  and getting only extension name

        String fileExtensionName = fileName.substring(fileName.indexOf("."));

        //Check condition if the file is xlsx file

        if(fileExtensionName.equals(".xlsx")){

            //If it is xlsx file then create object of XSSFWorkbook class

            try{
                if (inputStream!=null)
                    testWorkbook = new XSSFWorkbook(inputStream);}
            catch (Exception e){
                System.out.println("Exception - Trying to open reading of Excel 4, ExcelReader.Java|readExcel_Office");
            }

        }

        //Check condition if the file is xls file

        else if(fileExtensionName.equals(".xls")){

            //If it is xls file then create object of XSSFWorkbook class

            try{
                if (inputStream!=null)
                    testWorkbook = new HSSFWorkbook(inputStream);}
            catch (Exception e){
                System.out.println("Exception - Trying to open reading of Excel 5, ExcelReader.Java|readExcel_Office");
            }

        }

        //Read sheet inside the workbook by its name
        Sheet testSheet=null;
        if (testWorkbook!=null) {
            testSheet = testWorkbook.getSheet(sheetName);
        }

        //Find number of rows in excel file

        if (testSheet!=null) {
            int rowCount = testSheet.getLastRowNum() - testSheet.getFirstRowNum();
            resultSet=new String[rowCount+1][testSheet.getRow(0).getLastCellNum()];


            //Create a loop over all the rows of excel file to read it

            for (int i = 0; i < rowCount + 1; i++) {

                Row row = testSheet.getRow(i);

                if (row != null) {

                    for (int j=0;j<row.getLastCellNum();j++)
                    {
                        if (row.getCell(j)!=null) {
                            resultSet[i][j] = row.getCell(j).toString();
                        }
                    }
                }

            }

            stopwatch.stop();
            long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            GlobalVariables.logger.info("Excel Reading Took: " + stopwatch);
            return resultSet;
        }
        else {
            long millis = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            GlobalVariables.logger.info("Excel Reading Took: " + stopwatch);
            return null;
        }



    }


        //Main function is calling readExcel function to read data from excel file

        public static void main(String...strings){


            ExcelReader objExcelFile = new ExcelReader();

            //Pair<String, String> keyValue = new ImmutablePair("key", "value");



            //Prepare the path of excel file

            String filePath = System.getProperty("user.dir")+"\\src\\Data";

            //Call read file method of the class to read data

            HashMap<String,String> resultSet=objExcelFile.readExcel_Office(System.getProperty("user.dir")+"\\src\\Data","General.xlsx","TESTDATA01","1265");

        }

}
