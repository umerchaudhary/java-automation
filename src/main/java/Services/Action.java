package Services;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;


import java.util.List;
import java.util.concurrent.TimeUnit;

public class Action {
    private GlobalVariables GV;
    public Action(GlobalVariables GV2){
        GV=GV2;
    }
    private final By loadingObject=By.xpath("//div[" +
            "text()='loading chart...' " +
            "or (@id='searching-msg' and contains(text(),'Searching...')) " +
            "or (contains(@class,'blockUI') and not(contains(@style,'display:none')) and not(contains(@style,'display: none')) and not(contains(@style,'border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; background-color: rgb(0, 0, 0); opacity: 0.6; cursor: wait; position: fixed;')) ) " +
            "or @class='loading_bg' or (contains(@class,'blockMsg') and not(contains(@style,'display: none')) ) " +
            "or (contains(@class,'blockElement') and not(contains(@style,'display: none')) ) " +
            "or contains(@class,'blockPage') " +
            "or (@class='dataTables_processing' and @style='visibility: visible;') " +
            "or (@id='mapsearch-searching-message' and (@style='top: 310px;' or @style='top: 547.844px;') ) " +
            "or (@id='divOverlay' and @class='overlay' and @style='display: block;')" +
            " ]");



    //***********************************************************
    //*************TEXTBOX FUNCTIONS******************************
    //***********************************************************


    public void setTextBoxValue(WebElement obj, String data) {
        if (obj != null) {
            obj.clear();
            obj.sendKeys(Keys.HOME);
            obj.sendKeys(data);
            GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action: </b>Written <br /><b>Data: </b>" + data);

        }
    }


    public void setTextBoxValuePassword(WebElement obj, String data) {
        if (obj != null) {
            obj.clear();
            obj.sendKeys(Keys.HOME);
            obj.sendKeys(data);
            GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action: </b>Written <br /><b>Data: </b>" + data.substring(0,1)+"******"+data.substring(data.length()-1));

        }
    }
    public String getText(WebElement obj){
        return obj.getText();
    }

    //***********************************************************
    //*************GRID / TABLE FUNCTIONS************************
    //***********************************************************

    public void clickGridItem(WebElement obj,int rowId,int colId) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.xpath("./*/tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }
            GV.CF.logInfo(tableColumns.get(colId), "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action: </b>Click <br /><b>Row: </b>" + rowId + "<br /><b>Column: </b>" + colId);
            GV.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            List<WebElement> items = tableColumns.get(colId).findElements(By.xpath("*"));
            GV.driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);
            if (items.size() > 0) {
                items.get(0).click();
            } else {
                tableColumns.get(colId).click();
            }
            loadChecker();

        }
    }

    public String getCellValue(WebElement obj,int rowId,int colId) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }
            return tableColumns.get(colId).getText();
        }
        else
            return null;
    }

    public WebElement getCell(WebElement obj,int rowId,int colId) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }
            return tableColumns.get(colId);
        }
        else
            return null;
    }


    public int getColumnNumberByName(WebElement obj,String Name) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            tableColumns = tableRows.get(0).findElements(By.tagName("th"));
            if (tableColumns.size()==0)
                tableColumns = tableRows.get(0).findElements(By.tagName("td"));
            for (int i=0;i<tableColumns.size();i++){
                if (tableColumns.get(i).getText().equals(Name))
                    return i;
            }
        }
        else
            return -1;
        return -1;
    }

    public int getRowNoByText(WebElement obj,int columnNo,String Text) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableRows = obj.findElements(By.xpath("./*/tr"));
            for (int i=0;i<tableRows.size();i++){
                List<WebElement> objs=tableRows.get(i).findElements(By.xpath("./td["+columnNo+"]"));
                if (objs.size()>0 && objs.get(0).getText().equals(Text))
                    return i;
            }
        }
        else
            return -1;
        return -1;
    }


    public String getCellClass(WebElement obj,int rowId,int colId) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }
            return tableColumns.get(colId).getAttribute("class");
        }
        else
            return null;
    }


    public String getCellLink(WebElement obj,int rowId,int colId) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }
            return (tableColumns.get(colId).findElement(By.tagName("a")).getAttribute("href"));

        }
        else
            return null;
    }

    public int findLastRowNumber(WebElement obj) {
        if (obj != null) {
            loadChecker();
            List<WebElement> tableRows = obj.findElements(By.tagName("tr"));
            return (tableRows.size() - 1);
        }
        else
            return -1;
    }


    //***********************************************************
    //*************DROPDOWN FUNCTIONS************************
    //***********************************************************

    public String getDropDownValue(WebElement obj) {
        if (obj != null) {
            Select drpLARshow = new Select(obj);
            return drpLARshow.getFirstSelectedOption().getText();
        }
        else
            return null;
    }


    public void setDropDownValue(WebElement obj, String data) {
        if (obj != null) {
            Select dropDownObj = new Select(obj);
            dropDownObj.selectByVisibleText(data);
            int nextLineChar=obj.findElement(By.xpath("./..")).getText().indexOf("\n");
            String ObjName = obj.findElement(By.xpath("./..")).getText();
            if (nextLineChar>0)
                ObjName = ObjName.substring(0, obj.findElement(By.xpath("./..")).getText().indexOf("\n"));
            GV.CF.logInfo(obj, "<b>Object: </b>" + ObjName + "<br /><b>Action: </b> Selection<br /><b>Data: </b>" + data);
            loadChecker();
        }
    }


    //***********************************************************
    //*************CHECKBOX FUNCTIONS************************
    //***********************************************************

    public void setCheckBox(WebElement obj,boolean data) {
        if (obj != null) {
            if (obj.getAttribute("checked") == null) {
                if (data) {
                    obj.click();
                    GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action:</b> Check/unCheck <br /><b>Data: </b> checked");
                } else {
                    GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action:</b> Check/unCheck <br /><b>Data: </b> Checkbox already un-checked");
                }
            } else {
                if (!data) {
                    obj.click();
                    GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action:</b> Check/unCheck <br /><b>Data: </b> un-checked");
                } else {
                    GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action:</b> Check/unCheck <br /><b>Data: </b> Checkbox already checked");
                }
            }


        }
    }


    //***********************************************************
    //*************MOUSE FUNCTIONS************************
    //***********************************************************

    public void mouseHover(WebElement obj){

        Actions action = new Actions(GV.driver);
        action.moveToElement(obj).build().perform();
    }

    //***********************************************************
    //*********************HELPER FUNCTIONS**************************
    //***********************************************************
    public void click(WebElement obj) {
        if (obj != null) {
            loadChecker();
            GV.CF.logInfo(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Action: </b> Click");
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            try{obj.click();}
            catch (Exception e){
                try{jse.executeScript("arguments[0].click()", obj);}
                catch (Exception ex){
                    GV.CF.logInfo(null, ex.getMessage());
                }
            }
            loadChecker();
        }
    }

    public String getObjCssValue(String obj,String Value) {
        if (obj != null) {
            loadChecker();
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            return ((String) jse.executeScript("return $(\"" + obj + "\").css('" + Value + "')", obj));
        }
        else
            return null;
    }

    public void loadChecker(){
        int count=0;
        if(!GV.CF.isAlertPresent()){
            //wait.IgnoreExceptionTypes(typeof(WebDriverTimeoutException));
            int flag = 0;
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("return document.readyState").toString().equals("complete");
            while (flag == 0 && !jse.executeScript("return document.readyState").toString().equals("complete")) {


                GV.logger.info("WAITING FOR PAGE LOADING TO COMPLETE");
                GV.CF.delay(500);
                count=count+1;
                if (count>=200){
                    GV.CF.logWarning(null,"Timeout loading for page using readyState");
                    flag=1;
                }

            }
            count=0;
            flag=0;

            while (flag == 0) {
                GV.driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
                List<WebElement> result = GV.driver.findElements(loadingObject);
                GV.driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);
                try {
                    if (result.size() > 0) {
                        for (int i = 0; i < result.size(); i++) {
                            if (result.get(i).getText().toLowerCase().contains("enter a new group name") || result.get(i).getText().equals("Are you sure you want to delete group \"zzAutoGroup\"?\n" +"Yes No"))
                                flag = 1;
                        }
                    }

                    //wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingObject));
                    if ( (result.size() == 0) || (result.size() == 1 && result.get(0).isDisplayed()==false)) {
                        flag = 1;
                    }
                /*if (result.size() == 1 && result.get(0).isDisplayed()==false) {
                    flag = 1;
                }*/
                    else{
                        try{
                            GV.logger.info("WAIT OBJECT: " + result.get(0).getAttribute("outerHTML"));
                            GV.CF.delay(500);}
                        catch (StaleElementReferenceException e) {
                            GV.CF.logInfo(null, "Wait Element cleared - " + loadingObject.toString());
                        } catch (Exception e) {
                            GV.CF.logInfo(null, "Exception - Trying to log waiting object, Action.Java");
                        }
                    }

                }catch (StaleElementReferenceException e){

                }

                count=count+1;
                if (count>=200){
                    GV.CF.logWarning(null,"Timeout loading for page");
                    flag=1;
                    GV.driver.navigate().refresh();
                }

                /*if (result.size()>0) {
                    for (int i = 0; i < result.size(); i++) {
                        if (result.get(i).getAttribute("outerHTML").contains("display:none") == true)
                            flag = 1;
                    }
                }*/
            }
        }
    }

    public void loadChecker(By xpath){
        int count=0;
        if(!GV.CF.isAlertPresent()){
            //wait.IgnoreExceptionTypes(typeof(WebDriverTimeoutException));
            int flag = 0;
            while (flag == 0) {
                GV.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
                List<WebElement> result = GV.driver.findElements(xpath);
                GV.driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);
                //wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingObject));
                if (result.size() == 0) {
                    flag = 1;
                } else {
                    try {
                        GV.logger.info("WAIT OBJECT: " + result.get(0).getAttribute("outerHTML"));
                        GV.CF.delay(500);
                    } catch (StaleElementReferenceException e) {
                        GV.CF.logInfo(null, "Waitf Element cleared - " + xpath.toString());
                    } catch (Exception e) {
                        GV.CF.logInfo(null, "Exception - Trying to log waiting object, Action.Java");
                    }
                }
                count=count+1;
                if (count>=200){
                    GV.CF.logWarning(null,"Timeout loading for page");
                    flag=1;
                    GV.driver.navigate().refresh();
                }

                /*if (result.size()>0) {
                    for (int i = 0; i < result.size(); i++) {
                        if (result.get(i).getAttribute("outerHTML").contains("display:none") == true)
                            flag = 1;
                    }
                }*/
            }
        }
    }

    public void refreshPage(){
        GV.driver.navigate().refresh();
        GV.CF.logInfo(null, "<b>Action: </b>Page Refreshed<br /><b>Data: </b>" + GV.driver.getCurrentUrl());
        loadChecker();
    }



    public String getAttribute(WebElement obj,String attribute) {
        if (obj != null) {
            return obj.getAttribute(attribute);
        }
        else
            return null;
    }

    public void acceptAlert(){
        GV.driver.switchTo().alert().accept();
    }

    public void dismissAlert(){
        GV.driver.switchTo().alert().dismiss();
    }

}
