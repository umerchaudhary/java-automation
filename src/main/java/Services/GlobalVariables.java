package Services;

import Drivers.*;
import Reports.ExtentManager;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.browserstack.local.Local;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class GlobalVariables {
    public WebDriver driver;
    public ExtentTest test;
    public ExtentTest testMain;
    public ExtentTest testSuite;
    public ExtentTest testTest;
    public ExtentTest testStep;
    public ExtentTest testSubStep;
    public ExtentReports extent;
    public String className;
    public static volatile ExtentManager extentManager;
    public SoftAssert softAssertion;
    public static Logger logger;
    public final String logLevel;
    public CommonFunc CF;
    public String MLSLink;
    public String MLS;
    public String superUser;
    public String superPassword;
    public final String broker;
    public final String office;
    public String rDeskURL;
    public String LARagentLink;
    public String LARrDeskUser;
    public String LARrDeskPassword;
    public String LARrDeskFirstName;
    public String LARrDeskLastName;
    public String LPRagentLink;
    public String LPRrDeskUser;
    public String LPRrDeskPassword;
    public String LPRrDeskFirstName;
    public String LPRrDeskLastName;
    public String MARagentLink;
    public String MARrDeskUser;
    public String MARrDeskPassword;
    public String MARrDeskFirstName;
    public String MARrDeskLastName;
    public String rDeskFirstName;
    public String rDeskLastName;
    public String AdminURL;
    public final String AdminUser;
    public final String AdminPassword;
    public String rWebURL;
    public final String LARrDeskUserPersonId;
    public final String LPRrDeskUserPersonId;
    public String MARrDeskUserPersonId;
    public String environment=null;
    public static DataAccess.Environment US_DB;
    public static DataAccess.Environment LW_DB;
    public static DataAccess.Environment WEB_DB;
    public static DataAccess.Environment rDesk_DB;
    public static DataAccess.Environment TRACK_DB;
    public static DataAccess.Environment DATAVAULT_DB;
    public static DataAccess.Environment LSA_DB;
    public String testName;
    public String recoId=null;
    public final String[] EnvironmentSection01={"1","17","5000","5500","10000","15000","30000","35000","35001","35002","35003","35004","35005","35006","35007","35008","35009","35010","35011","35012","35014","35015","35016","40000","45000","55000","9000001","9000002","9100001","9100002","9100003","9100004","9100005","9100006"};
    public HashMap<String,String> resultSetExcel;
    public DataProcessor dataProcessor;
    public static Boolean jenkinsFlag=false;
    public Boolean getLatestDataFromDB=null;
    public String browser;
    public static String platform=null;
    public boolean firstRunFlag=true;
    public int objTimeout=20;
    public Date tempDate=null;
    public HashMap<String, String> testData=new HashMap<String, String>();
    public HashMap<String, Integer> recoTier;
    public boolean skipFlag=false;
    public String suiteName=null;
    public String sqlDBDate=null;
    public Local bsLocal;


    public GlobalVariables(ExtentManager extentManager2) {
        //extentManager=new ExtentManager(this);
        dataProcessor=new DataProcessor(this);
        CF=new CommonFunc(this);
        Properties obj = new Properties();
        //systemVariablesUpdate();
        try {
            FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\application.properties");
            obj.load(objfile);
        }
        catch (Exception e){
            System.out.println("Exception - Trying to read application.properties file, GlobalVariables.Java|GlobalVariables");
        }
        systemVariablesUpdate(obj);
        recoTierUpdate();
        className="";
        MLSLink = obj.getProperty("MLSLink");
        MLS=obj.getProperty("MLS");
        rDeskURL=obj.getProperty("rDeskURL");
        superUser=obj.getProperty("superUser");
        superPassword=obj.getProperty("superPassword");
        broker=obj.getProperty("broker");
        office=obj.getProperty("office");
        LARagentLink=obj.getProperty("LARagentLink");
        LARrDeskUser=obj.getProperty("LARrDeskUser");
        LARrDeskUserPersonId=obj.getProperty("LARrDeskUserPersonId");
        LARrDeskPassword=obj.getProperty("LARrDeskPassword");
        LARrDeskFirstName=obj.getProperty("LARrDeskFirstName");
        LARrDeskLastName=obj.getProperty("LARrDeskLastName");
        LPRagentLink=obj.getProperty("LPRagentLink");
        LPRrDeskUser=obj.getProperty("LPRrDeskUser");
        LPRrDeskPassword=obj.getProperty("LPRrDeskPassword");
        LPRrDeskFirstName=obj.getProperty("LPRrDeskFirstName");
        LPRrDeskLastName=obj.getProperty("LPRrDeskLastName");
        LPRrDeskUserPersonId=obj.getProperty("LPRrDeskUserPersonId");
        MARagentLink=obj.getProperty("MARagentLink");
        MARrDeskUser=obj.getProperty("MARrDeskUser");
        MARrDeskPassword=obj.getProperty("MARrDeskPassword");
        MARrDeskFirstName=obj.getProperty("MARrDeskFirstName");
        MARrDeskLastName=obj.getProperty("MARrDeskLastName");
        MARrDeskUserPersonId=obj.getProperty("MARrDeskUserPersonId");
        if (getLatestDataFromDB==null)
            getLatestDataFromDB= Boolean.valueOf(obj.getProperty("getLatestDataFromDB"));
        rWebURL=obj.getProperty("rWebURL");
        AdminURL=obj.getProperty("AdminURL");
        AdminUser=obj.getProperty("AdminUser");
        AdminPassword=obj.getProperty("AdminPassword");
        logLevel=obj.getProperty("logLevel");
        if (environment==null) {
            environment = obj.getProperty("Environment");
        }
        if (platform==null){
            platform=obj.getProperty("platform");
        }
        if (environment.equals("DEV")){
            US_DB=DataAccess.Environment.STG_US;
            LW_DB=DataAccess.Environment.STG_ListingWarehouse;
            LSA_DB=DataAccess.Environment.DEV_ListingSearchActivity;
            rDesk_DB=DataAccess.Environment.DEV_rDESK;
            DATAVAULT_DB=DataAccess.Environment.DEV_DATAVAULT;
            if (findInArray(EnvironmentSection01,recoId)) {
                WEB_DB = DataAccess.Environment.DEV_WEB01;
                TRACK_DB = DataAccess.Environment.DEV_Tracking01;
            }
            else{
                WEB_DB = DataAccess.Environment.DEV_WEB02;
                TRACK_DB = DataAccess.Environment.DEV_Tracking02;
            }
        }
        else if (environment.equals("STAGE")) {
            US_DB=DataAccess.Environment.STG_US;
            LW_DB=DataAccess.Environment.STG_ListingWarehouse;
            LSA_DB=DataAccess.Environment.STG_ListingSearchActivity;
            DATAVAULT_DB=DataAccess.Environment.STAGE_DATAVAULT;
            rDesk_DB=DataAccess.Environment.STG_rDESK;
            if (findInArray(EnvironmentSection01,recoId)) {
                WEB_DB = DataAccess.Environment.STG_WEB01;
                TRACK_DB = DataAccess.Environment.STG_Tracking01;
            }
            else{
                WEB_DB = DataAccess.Environment.STG_WEB02;
                TRACK_DB = DataAccess.Environment.STG_Tracking02;
            }

        }
        else if (environment.equals("QA")) {
            US_DB=DataAccess.Environment.STG_US;
            LW_DB=DataAccess.Environment.STG_ListingWarehouse;
            LSA_DB=DataAccess.Environment.STG_ListingSearchActivity;
            DATAVAULT_DB=DataAccess.Environment.STAGE_DATAVAULT;
            rDesk_DB=DataAccess.Environment.QA_rDESK;
            if (findInArray(EnvironmentSection01,recoId)) {
                WEB_DB = DataAccess.Environment.QA_WEB01;
                TRACK_DB = DataAccess.Environment.QA_Tracking01;
            }
            else{
                WEB_DB = DataAccess.Environment.QA_WEB02;
                TRACK_DB = DataAccess.Environment.QA_Tracking02;
            }

        }
        else if (environment.equals("PRODUCTION")) {
            //US_DB=DataAccess.Environment.STG_US;
            //LW_DB=DataAccess.Environment.STG_ListingWarehouse;
            //LSA_DB=DataAccess.Environment.STG_ListingSearchActivity;
            //DATAVAULT_DB=DataAccess.Environment.STAGE_DATAVAULT;
            rDesk_DB=DataAccess.Environment.PROD_rDESK;
            if (findInArray(EnvironmentSection01,recoId)) {
                WEB_DB = DataAccess.Environment.PROD_WEB01;
                TRACK_DB = DataAccess.Environment.PROD_Tracking01;
            }
            else{
                WEB_DB = DataAccess.Environment.PROD_WEB02;
                TRACK_DB = DataAccess.Environment.PROD_Tracking02;
            }

        }
        else if (environment.equals("HOTFIX")) {
            //US_DB=DataAccess.Environment.STG_US;
            //LW_DB=DataAccess.Environment.STG_ListingWarehouse;
            //LSA_DB=DataAccess.Environment.STG_ListingSearchActivity;
            //DATAVAULT_DB=DataAccess.Environment.STAGE_DATAVAULT;
            rDesk_DB=DataAccess.Environment.HOTFIX_rDESK;
            if (findInArray(EnvironmentSection01,recoId)) {
                WEB_DB = DataAccess.Environment.HOTFIX_WEB01;
                TRACK_DB = DataAccess.Environment.HOTFIX_Tracking01;
            }
            else{
                WEB_DB = DataAccess.Environment.HOTFIX_WEB02;
                TRACK_DB = DataAccess.Environment.HOTFIX_Tracking02;
            }

        }
        else
            System.out.println("FAILED: UNIDENTIFIED DB SOURCE");

        setBrowser();
        extentManager=extentManager2;
        extent=extentManager.GetExtent();



    }

    public ExtentTest getTest(){
        return test;
    }

    public Boolean findInArray(String[] arr,String data){
        for (int i=0;i<arr.length;i++){
            if (arr[i].equals(data))
                return true;
        }
        return false;

    }
    private void systemVariablesUpdate(Properties obj){
        if (System.getProperty("jenkins")!=null && Boolean.valueOf(System.getProperty("jenkins")))
            jenkinsFlag = true;
        else
            jenkinsFlag=Boolean.valueOf(obj.getProperty("jenkins"));
        if (System.getProperty("browser")!=null)
            browser = System.getProperty("browser");
        else
            browser=obj.getProperty("browser");
        if (System.getProperty("recoId")!=null)
            recoId = System.getProperty("recoId");
        else
            recoId=obj.getProperty("RecoId");
        if (System.getProperty("getLatestDataFromDB")!=null && Boolean.valueOf(System.getProperty("getLatestDataFromDB")))
            getLatestDataFromDB = true;
        if (System.getProperty("environment")!=null)
            environment = System.getProperty("environment");
        if (System.getProperty("platform")!=null)
            platform = System.getProperty("platform");


    }

    private void recoTierUpdate(){
        recoTier= new HashMap<String, Integer>();
        recoTier.put("1265",4);
        recoTier.put("1201",4);
        recoTier.put("9000002",3);
        recoTier.put("35014",3);
        recoTier.put("35009",3);
        recoTier.put("1321",3);
    }

    private void setBrowser(){
        if (this.browser.toLowerCase().equals("chrome")) {
            Chrome chromeHandler = new Chrome(0);
            driver = chromeHandler.chromeDriver();
        }
        if (this.browser.toLowerCase().equals("chrome beta")) {
            Chrome chromeHandler = new Chrome(1);
            driver = chromeHandler.chromeDriver();
        }
        if (this.browser.toLowerCase().contains("browserstack")) {
            BrowserStack chromeHandler = new BrowserStack(browser,0,platform);
            driver = chromeHandler.chromeDriver();
            bsLocal=chromeHandler.browserStackLocal();
        }

        if (this.browser.toLowerCase().equals("firefox")) {
            Firefox firefoxHandler = new Firefox();
            driver = firefoxHandler.firefoxDriver();
        }
        if (this.browser.toLowerCase().equals("edge")) {
            Edge edgeHandler = new Edge();
            driver = edgeHandler.edgeDriver();
        }
        this.driver=driver;
        driver.manage().timeouts().implicitlyWait(this.objTimeout, TimeUnit.SECONDS);

    }



}
