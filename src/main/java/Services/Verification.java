package Services;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Verification {
    private GlobalVariables GV;
    ExtentReports extent;
    ExtentTest test;

    public Verification(GlobalVariables GV2){
        GV=GV2;
    }


    //***********************************************************
    //*************TITLE FUNCTIONS************************
    //***********************************************************
    public void Title(String data){
        GV.softAssertion.assertEquals(GV.driver.getTitle(),data);
        if (GV.driver.getTitle().equals(data)){
            GV.CF.logPass(null,"<b>Object: </b> Title <br /><b>Verification: </b>" + data);

        }
        else{
            GV.CF.logFail(null,"<b>Object: </b>Title <br /><b>Verification: </b> Failed. <br />Expected: " + data + "<br />Actual: " + GV.driver.getTitle());
        }
    }

    //***********************************************************
    //*************IMAGE COMPARISON FUNCTIONS************************
    //***********************************************************
    public void compareImages(WebElement obj, String path) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            String coordinates[] = {"", ""};
            coordinates[0] = GV.CF.coordinatesFetch(obj, "x");
            coordinates[1] = GV.CF.coordinatesFetch(obj, "y");
            BufferedImage expectedImage = null;
            try {
                expectedImage = ImageIO.read(new File(System.getProperty("user.dir") + path));
            } catch (Exception e) {
                System.out.println(e.getMessage());

            }

            // Get entire page screenshot
            File screenshot = ((TakesScreenshot) GV.driver).getScreenshotAs(OutputType.FILE);
            BufferedImage fullImg = null;
            try {
                fullImg = ImageIO.read(screenshot);
            } catch (Exception e) {
                System.out.println("Exception - Trying to read Screenshot, Verification.Java|compareImages");
            }

            // Get the location of element on the page
            Point point = new Point(0, 0);
            point.x = (int) Math.round(Double.valueOf(coordinates[0]));
            point.y = (int) Math.round(Double.valueOf(coordinates[1]));

            // Get width and height of the element
            int eleWidth = obj.getSize().getWidth();
            int eleHeight = obj.getSize().getHeight();

            // Crop the entire page screenshot to get only element screenshot
            BufferedImage actualImage = null;
            if (fullImg!=null)
                actualImage = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
            //Screenshot logoImageScreenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(GV.driver, obj);
            //BufferedImage actualImage = logoImageScreenshot.getImage();

            ImageDiffer imgDiff = new ImageDiffer();
            ImageDiff diff=null;
            if (actualImage!=null && expectedImage!=null) {
                diff = imgDiff.makeDiff(actualImage, expectedImage);
                GV.softAssertion.assertFalse(diff.withDiffSizeTrigger(5000).hasDiff(), "Images are Same");
                if (!diff.withDiffSizeTrigger(5000).hasDiff()) {
                    GV.CF.logPass(obj, "Image Verified : " + path);
                } else {
                    try {
                        GV.CF.logFail(obj, "Image Verification failed : " + path);
                        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss ");
                        Date date = new Date();
                        String date1 = dateFormat.format(date);
                        String imagePath = System.getProperty("user.dir") + "\\test-output\\extentReports\\" + date1 + "_1.png";
                        ImageIO.write(diff.getMarkedImage(), "png", new File(imagePath));
                        GV.test.addScreenCaptureFromPath(imagePath);
                        imagePath = System.getProperty("user.dir") + "\\test-output\\extentReports\\" + date1 + "_2.png";
                        ImageIO.write(expectedImage, "png", new File(imagePath));
                        GV.test.addScreenCaptureFromPath(imagePath);
                        imagePath = System.getProperty("user.dir") + "\\test-output\\extentReports\\" + date1 + "_3.png";
                        ImageIO.write(actualImage, "png", new File(imagePath));
                        GV.test.addScreenCaptureFromPath(imagePath);
                    } catch (Exception e) {
                        System.out.println("Exception - Trying to write Screenshot, Verification.Java|compareImages");
                    }
                }
            }

        }
    }

    public void vHighLightElement(RemoteWebDriver driver, WebElement weElement) {
        driver.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
                weElement);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            GV.CF.logFail(null,e.getMessage());
        }
        driver.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", weElement);
    }

    public void verifyObjText(WebElement obj,String name,String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getText().replace(" \n","\n").trim(), data.replace(" \n","\n").replace("&amp;","&").trim());
            if (obj.getText().replace(" \n","\n").trim().compareTo(data.replace(" \n","\n").replace("&amp;","&").trim()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }

    public void verifyObjTextCaseInsensitive(WebElement obj,String name,String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getText().replace(" \n","\n").trim().toLowerCase(), data.replace(" \n","\n").replace("&amp;","&").trim().toLowerCase());
            if (obj.getText().replace(" \n","\n").trim().toLowerCase().compareTo(data.replace(" \n","\n").replace("&amp;","&").trim().toLowerCase()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }

    public void verifyObjTextWithoutNewLine(WebElement obj,String name,String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getText().replace(" \n","\n").replace("\n","").trim(), data.replace(" \n","\n").replace("\n","").replace("&amp;","&").trim());
            if (obj.getText().replace(" \n","\n").replace("\n","").trim().compareTo(data.replace(" \n","\n").replace("\n","").replace("&amp;","&").trim()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }

    public void verifyObjPartialText(WebElement obj,String name,String data) { //NOT WORKING
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertTrue(obj.getText().trim().contains(data.trim()));
            if (obj.getText().trim().contains(data.trim())) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }
    public void verifyObjPartialTextCaseInsensitive(WebElement obj,String name,String data) { //NOT WORKING
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertTrue(obj.getText().trim().toLowerCase().contains(data.trim().toLowerCase()));
            if (obj.getText().trim().toLowerCase().contains(data.trim().toLowerCase())) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }

    public void verifyObjDateText(WebElement obj,String name,String data) {
        int DateExp=0,DateActual=0;
        if (obj != null) {
            Pattern p = Pattern.compile("\\d+");
            Matcher m1 = p.matcher(data);
            Matcher m2 = p.matcher(obj.getText());
            if (m1.find()) {
                DateExp = Integer.valueOf(m1.group());
            }
            if (m2.find()) {
                DateActual = Integer.valueOf(m2.group());
            }

            if ( Math.abs(DateExp-DateActual)==1){
                data=data.replace(String.valueOf(DateExp),String.valueOf(DateActual));
            }
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getText().replace(" \n","\n").trim(), data.replace(" \n","\n").trim());
            if (obj.getText().replace(" \n","\n").trim().compareTo(data.replace(" \n","\n").trim()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getText().trim());
            }
        }
    }

    public void verifyObjAttribute(WebElement obj,String name,String attribute,String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getAttribute(attribute).trim(), data.trim());
            if (obj.getAttribute(attribute).trim().compareTo(data.trim()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.trim() + "<br /><b>Actual: </b>" + obj.getAttribute(attribute).trim());
            }
        }
    }

    public void verifyObjTextAllLower(WebElement obj,String name,String data) {
        data=data.toLowerCase();
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getText().replace(" \n","\n").toLowerCase().trim(), data.replace(" \n","\n").toLowerCase().trim());
            if (obj.getText().replace(" \n","\n").toLowerCase().trim().compareTo(data.replace(" \n","\n").toLowerCase().trim()) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + name + "<br /><b>Verification: </b>" + data.trim());
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + name + "<br /><b>Expected: </b>" + data.replace(" \n","\n").trim() + "<br /><b>Actual: </b>" + obj.getText().replace(" \n","\n").toLowerCase().trim());
            }
        }
    }

    public void verifyObjClass(WebElement obj,String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getAttribute("class"), data);
            if (obj.getAttribute("class").compareTo(data) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification: </b>" + data);
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Expected: </b>" + data + "<br /><b>Actual: </b>" + obj.getAttribute("class"));
            }
        }
    }

    public void verifyObjPicExist(WebElement obj,String path) {
        if (obj != null) {
            boolean imageComparisonResult;
            boolean coordinatesComparisonResult;
            coordinatesComparisonResult = obj.getSize().getWidth() > 0 && obj.getSize().getHeight() > 0;
            GV.softAssertion.assertEquals(coordinatesComparisonResult, true, "Image size greater than 0");
            if (coordinatesComparisonResult) {
                if (path.compareTo("") == 0) {
                    GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "Picture" + "<br /><b>Verification: </b>" + "Exists");
                } else {
                    imageComparisonResult = GV.CF.compareImagesNoLog(obj, path);
                    GV.softAssertion.assertEquals(imageComparisonResult, false, "Image does not match default -No Image- of Company");
                    if (imageComparisonResult) {
                        GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "Picture" + "<br /><b>Expected: </b>" + "Exists" + "<br /><b>Actual: </b>" + "Not Exists");
                    } else {
                        GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "Picture" + "<br /><b>Verification: </b>" + "Exists");
                    }
                }
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "Picture" + "<br /><b>Expected: </b>" + "Exists" + "<br /><b>Actual: </b>" + "Not Exists");
            }

        }
    }

    //***********************************************************
    //*************GRID/ TABLE FUNCTIONS************************
    //***********************************************************

    public void verifyCellValue(WebElement obj,int rowId,int colId,String data) {
        if (obj != null) {
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.xpath("./*/tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }

            if (data == null) {
                data = "";
            }
            GV.softAssertion.assertEquals(tableColumns.get(colId).getText().trim(), data.trim());
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", tableColumns.get(colId));
            if (tableColumns.get(colId).getText().trim().compareTo(data.trim()) == 0) {
                GV.CF.logPass(tableColumns.get(colId), "Text verification successful as <b>" + data.trim() + "</b><br /> <b>Row: </b>" + rowId + "<br /><b>Column: </b>" + colId);
            } else {
                GV.CF.logFail(tableColumns.get(colId), "Text verification failed <br /><b> Expected:</b>" + data.trim() + "<br /> <b>Actual:</b>" + tableColumns.get(colId).getText().trim() + "</b><br> <b>Row: </b>" + rowId + "<br><b>Column: </b>" + colId);
            }
        }
    }

    public void verifyCellClassName(WebElement obj,int rowId,int colId,String data) {
        if (obj != null) {
            int flag = 0;
            List<WebElement> tableColumns;
            List<WebElement> tableRows = obj.findElements(By.xpath("./*/tr"));
            if (rowId == 0) {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("th"));
            } else {
                tableColumns = tableRows.get(rowId).findElements(By.tagName("td"));
            }

            if (data == null) {
                data = "";
            }
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", tableColumns.get(colId));
            List<WebElement> Objs = tableColumns.get(colId).findElements(By.tagName("i"));
            for (int i = 0; i < Objs.size(); i++) {
                if (Objs.get(i).getAttribute("class").trim().compareTo(data) == 0) {
                    GV.CF.logPass(tableColumns.get(colId), "Text verification successful as <b>" + data + "</b> on <br> <b>Row: </b>" + rowId + "<br><b>Column: </b>" + colId);
                    GV.softAssertion.assertEquals(Objs.get(i).getAttribute("class").trim(), data);
                    flag = 1;
                }
            }
            if (flag == 0) {
                GV.CF.logFail(tableColumns.get(colId), "Text verification failed <br /> Expected:" + data + "<br /> Actual:" + Objs.get(0).getAttribute("class"));
                GV.softAssertion.assertEquals(Objs.get(0).getAttribute("class").trim(), data);
            }
        }
    }


    //***********************************************************
    //*************OBJECT EXISTANCE FUNCTIONS************************
    //***********************************************************

    public void verifyExists(By identifier,boolean test){
        GV.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        List<WebElement> ShowHelp = GV.CF.findElements(identifier);
        GV.driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);
        if (ShowHelp.size()>0 && ShowHelp.get(0).isDisplayed()){
            if (test) {
                GV.CF.logPass(ShowHelp.get(0),"<b>Object: </b>" + identifier.toString() + "<br /><b>Verification: </b>  Exists ");
            }
            else {
                GV.CF.logFail(ShowHelp.get(0),"<b>Object: </b>" + identifier.toString() + "<br /><b>Verification :</b> does not Exists ");
            }
            GV.softAssertion.assertEquals(test, ShowHelp.get(0).isDisplayed());
        }
        else{
            if (!test) {
                GV.CF.logPass(null,"<b>Object: </b>" + identifier.toString() + "<br /><b>Verification :</b> does not Exists ");
            }
            else {
                GV.CF.logFail(null,"<b>Object: </b>" + identifier.toString() + "<br /><b>Verification: </b>  Exists ");
            }
            GV.softAssertion.assertEquals(test, (ShowHelp.size()>0 && ShowHelp.get(0).isDisplayed()));

        }

    }

    public Boolean verifyExists(By identifier){
        GV.driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        List<WebElement> ShowHelp = GV.CF.findElements(identifier);
        GV.driver.manage().timeouts().implicitlyWait(GV.objTimeout, TimeUnit.SECONDS);
        return (ShowHelp.size()>0);

    }

    //***********************************************************
    //*************LINK FUNCTIONS************************
    //***********************************************************
    public void verifyObjectLink(WebElement obj, String data) {
        if (obj != null) {
            JavascriptExecutor jse = (JavascriptExecutor) GV.driver;
            jse.executeScript("arguments[0].scrollIntoView()", obj);
            GV.softAssertion.assertEquals(obj.getAttribute("href"), data);
            if (obj.getAttribute("href").compareTo(data) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification: </b>" + data);
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Expected: </b>" + data + "<br /><b>Actual: </b>" + obj.getAttribute("href"));
            }

        }
    }

    //***********************************************************
    //*************CHECKBOX FUNCTIONS************************
    //***********************************************************
    public void verifyCheckBoxState(WebElement obj,boolean data) {
        if (obj != null) {
            if (obj.getAttribute("checked") == null) {
                if (data) {
                    GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Check/unCheck <br /><b>Actual: </b> un-Checked <br /><b>Expected: </b> Checked");
                } else {
                    GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Check/unCheck <br /><b>Actual: </b> un-Checked <br /><b>Expected: </b> un-Checked");
                }
            } else {
                if (!data) {
                    GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Check/unCheck <br /><b>Actual: </b> Checked <br /><b>Expected: </b> un-Checked");
                } else {
                    GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Check/unCheck <br /><b>Actual: </b> Checked <br /><b>Expected: </b> Checked");
                }
            }

        }
    }

    //***********************************************************
    //*************DROPDOWN FUNCTIONS************************
    //***********************************************************
    public void verifyDropDownValue(WebElement obj,String data) {
        if (obj != null) {
            Select dropDownObj = new Select(obj);
            if (dropDownObj.getFirstSelectedOption().getText().compareTo(data) == 0) {
                GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Selected Value <br /><b>Actual: </b>" + dropDownObj.getFirstSelectedOption().getText() + "<br /><b>Expected: </b>" + data);
            } else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> Selected Value <br /><b>Actual: </b>" + dropDownObj.getFirstSelectedOption().getText() + "<br /><b>Expected: </b>" + data);
            }

        }
    }

    public void verifyDropDownValues(WebElement obj,List<String> data) {
        if (obj != null) {
            Select dropDownObj = new Select(obj);
            GV.softAssertion.assertEquals(dropDownObj.getOptions().size(),data.size());
            if (dropDownObj.getOptions().size()==data.size()){
                GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Items Count <br /><b>Actual: </b>" + dropDownObj.getOptions().size());
            }
            else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Items Count <br /><b>Actual: </b>" + dropDownObj.getOptions().size()  + "<br /><b>Expected: </b>" + data.size());
            }


            for (int i=0;i<dropDownObj.getOptions().size();i++){
                GV.softAssertion.assertTrue(data.contains(dropDownObj.getOptions().get(i).getText()));
                if (data.contains(dropDownObj.getOptions().get(i).getText())){
                    GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Item <br /><b>Actual: </b>" + dropDownObj.getOptions().get(i).getText());
                }
                else {
                    GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Item <br /><b>Actual: </b>" + dropDownObj.getOptions().get(i).getText());
                }
            }

        }
    }

    public void verifyDropDownValuesWithOrder(WebElement obj,Integer[][] data) {
        if (obj != null) {
            Select dropDownObj = new Select(obj);
            GV.softAssertion.assertEquals(dropDownObj.getOptions().size(),data.length);
            if (dropDownObj.getOptions().size()==data.length){
                GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Items Count <br /><b>Actual: </b>" + dropDownObj.getOptions().size());
            }
            else {
                GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Items Count <br /><b>Actual: </b>" + dropDownObj.getOptions().size()  + "<br /><b>Expected: </b>" + data.length);
            }
            int currentCount=0;
            int currentZip=0;
            for (int i=0;i<dropDownObj.getOptions().size();i++){
                currentCount=data[i][1];
                currentZip=data[i][0];
                if (! (currentZip+" (ZipCode)").equals(dropDownObj.getOptions().get(i).getText())) {
                    for (int j=0;j<dropDownObj.getOptions().size();j++){
                        if ((data[j][0]+" (ZipCode)").equals(dropDownObj.getOptions().get(i).getText()) && data[j][1]==currentCount){
                            currentZip=data[j][0];
                        }
                    }
                }
                GV.softAssertion.assertTrue( (currentZip+" (ZipCode)").equals(dropDownObj.getOptions().get(i).getText()));
                if ( (currentZip+" (ZipCode)").equals(dropDownObj.getOptions().get(i).getText())){
                    GV.CF.logPass(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Item <br /><b>Actual: </b>" + dropDownObj.getOptions().get(i).getText() +"<br /><b>Count: </b>" +currentCount);
                }
                else {
                    GV.CF.logFail(obj, "<b>Object: </b>" + GV.CF.getObjectDisplayName(obj) + "<br /><b>Verification:</b> DropDown Item <br /><b>Actual: </b>" + dropDownObj.getOptions().get(i).getText()+"<br /><b>Count: </b>" +currentCount);
                }
            }

        }
    }

}
