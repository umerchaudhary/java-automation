package Services;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.testng.Assert;
import org.testng.ITestResult;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CommonFunc {
    private GlobalVariables GV;
    private final By cookieBanner=By.xpath("//*[@class='btn-icon cookie-banner-close empty']");

    public final String[] suffixes =
            //    0     1     2     3     4     5     6     7     8     9
            { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    //    10    11    12    13    14    15    16    17    18    19
                    "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                    //    20    21    22    23    24    25    26    27    28    29
                    "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                    //    30    31
                    "th", "st" };

    public final String[] MonthNumber =
            //    0   1   2    3    4     5      6     7      8     9      10    11      12
            { "Jan", "Feb", "Mar", "Apr","May","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                     };


    public CommonFunc(GlobalVariables GV2){
        GV=GV2;
    }

    //************************************************************
    //**************GET OBJECT NAME FOR REPORTING****************
    //************************************************************
    public String getObjectDisplayName(WebElement obj){
        String Name=null;
        int flag=0;
        if (obj.getAttribute("type")!=null && (obj.getAttribute("type").compareTo("text")==0 || obj.getAttribute("type").compareTo("password")==0 || obj.getAttribute("type").compareTo("textarea")==0 || obj.getAttribute("type").compareTo("email")==0 || obj.getAttribute("type").compareTo("search")==0 || obj.getAttribute("type").compareTo("tel")==0 ) ){     //Type is Text OR Password
            flag=1;
            if (obj.getAttribute("placeholder").length()!=0) {
                Name=obj.getAttribute("placeholder");
            }
            else if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
            }
            else if(obj.getAttribute("name").length()!=0){
                Name=obj.getAttribute("name");
            }
            else if(obj.getAttribute("class").length()!=0){
                Name=obj.getAttribute("class");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }

        }
        if (flag==0 && obj.getAttribute("type")!=null && obj.getAttribute("type").compareTo("select-one")==0){     //Type is Text OR Password
            flag=1;
            if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
                flag=1;
            }
            else if(obj.getAttribute("name").length()!=0){
                Name=obj.getAttribute("name");
                flag=1;
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }

        }

        if (flag==0 && obj.getAttribute("type")!=null && (obj.getAttribute("type").compareTo("button")==0 || obj.getAttribute("type").compareTo("submit")==0 || obj.getAttribute("type").compareTo("image")==0)){     //Type is button OR submit
            flag=1;
            if (obj.getText().length()!=0) {
                Name=obj.getText();
            }
            else if(obj.getAttribute("value").length()!=0){
                Name=obj.getAttribute("value");
            }
            else if(obj.getAttribute("name").length()!=0){
                Name=obj.getAttribute("name");
            }
            else if(obj.getAttribute("class").length()!=0){
                Name=obj.getAttribute("class");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }

        }

        if (flag==0 && obj.getAttribute("type")!=null && (obj.getAttribute("type").compareTo("checkbox")==0 || obj.getAttribute("type").compareTo("radio")==0)){  //Type is Checkbox
            flag=1;
            if (obj.getAttribute("id").length()!=0) {
                Name=obj.getAttribute("id");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + "");
            }
        }

        if (flag==0 && obj.getAttribute("type")!=null && (obj.getAttribute("type").compareTo("")==0)){  //Type is empty
            flag=1;
            Name=obj.getText();
        }

        if (flag==0 && (obj.getTagName().compareTo("img")==0 || obj.getTagName().compareTo("h1")==0 ||  obj.getTagName().compareTo("h3")==0 || obj.getTagName().compareTo("h4")==0 || obj.getTagName().compareTo("h5")==0 || obj.getTagName().compareTo("h6")==0|| obj.getTagName().compareTo("p")==0)) {              //Type is paragraph OR Heading
            flag=1;
            if(obj.findElement(By.xpath("..")).getAttribute("data-item")!=null ){
                Name=obj.findElement(By.xpath("..")).getAttribute("data-item");
            }
            else if(obj.findElement(By.xpath("..")).getAttribute("id")!=null && obj.findElement(By.xpath("..")).getAttribute("id").length()!=0){
                Name=obj.findElement(By.xpath("..")).getAttribute("id");
            }
            else if(obj.findElement(By.xpath("..")).getAttribute("class")!=null && obj.findElement(By.xpath("..")).getAttribute("class").length()!=0){
                Name=obj.findElement(By.xpath("..")).getAttribute("class");
            }
            else if(obj.getText().length()>0){
                Name=obj.getText();
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }
        }

        if (flag==0 && obj.getTagName().compareTo("span")==0) {                                                                                         //Span
            flag=1;
            if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
            }
            else if(obj.getAttribute("class").length()!=0){
                Name=obj.getAttribute("class");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }
        }

        if (flag==0 && obj.getTagName().compareTo("div")==0) {                                                                                         //DIV
            flag=1;
            if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
            }
            else if(obj.getAttribute("class").length()!=0){
                Name=obj.getAttribute("class");
            }
            else if(obj.getAttribute("innerText").length()!=0){
                Name=obj.getAttribute("innerText");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }
        }

        if (flag==0 && obj.getTagName().compareTo("table")==0) {                                                                                         //TABLE
            flag=1;
            if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
            }
            else{
                Name="Table";
                //GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }
        }

        if (flag==0 && obj.getTagName().compareTo("select")==0){  //Type is DropDown
            flag=1;
            if(obj.getAttribute("id").length()!=0){
                Name=obj.getAttribute("id");
            }
            if(obj.getAttribute("name").length()!=0){
                Name=obj.getAttribute("name");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }
        }

        if (flag==0 && (obj.getTagName().equals("i") || obj.getTagName().equals("li")) ){
            flag=1;
            if(obj.getAttribute("class").length()!=0){
                Name=obj.getAttribute("class");
            }
            else{
                Name="Object Name Not Found";
                GV.test.log(Status.WARNING,"Object Name Not Found For:" + obj.findElement(By.xpath(".")).toString());
            }

        }

        if (flag==0 && obj.getAttribute("type")==null){
            flag=1;
            Name=obj.getText();
        }

        if (flag==0){//NOT FOUND
            if (obj.getAttribute("name")!=null && !obj.getAttribute("name").equals("")){
                Name=obj.getAttribute("name");
            }
            else
            GV.test.log(Status.WARNING,"Object Identification failed for :" + obj.findElement(By.xpath(".")).toString());
        }



        return Name;
    }

    public void getUrl(String url){
        if (GV.environment.equals("PRODUCTION") && GV.recoId.equals("9000002"))
            url=url.replace("http://","https://");
        if (GV.recoId.equals("35014") && (url.endsWith(".com")) && (url.contains(".realestatedigital.com") || (url.contains(".rdeskbw-stage.com")) || (url.contains(".rweb1-qa.com")) ))
            url=url+"/?s";
        logInfo(null,"Navigating to URL: " + url);
        int i=0;
        while (i==0) {
            try {
                GV.driver.get(url);
                i++;
            } catch (Exception ex) {
                logInfo(null,"Retrying to navigate to URL: " + url);
                GV.driver.get(url);
                i++;

            }
        }
        GV.CF.delay(5000);
    }

    public void logUrl(){
        logInfo(null,"Current URL: "+ GV.driver.getCurrentUrl());
    }

    public WebElement findElement(By finder){
        WebElement obj=null;
        int attempts = 0;
        while(attempts < 3) {
            try {
                obj=GV.driver.findElement(finder);
                break;
            } catch(StaleElementReferenceException e) {
                System.out.println("Stale Element Exception");
                attempts++;
            }
            catch(NoSuchElementException e) {
                logWithScreenshot(null, e.getMessage(), "FAIL");
                logInfo(null,"<b>Calling Function: </b>"+Thread.currentThread().getStackTrace()[2].getMethodName());
                logInfo(null,"<b>Calling Class: </b>"+Thread.currentThread().getStackTrace()[2].getClassName()+ " ("+Thread.currentThread().getStackTrace()[2].getLineNumber()+")");
                Assert.fail(e.getMessage());
                break;
            }
        }
        return obj;
    }

    public List<WebElement> findElements(By finder){
        List<WebElement> obj=null;
        int attempts = 0;
        while(attempts < 2) {
            try {
                obj=GV.driver.findElements(finder);
                break;
            } catch(StaleElementReferenceException e) {
                System.out.println("Stale Element Exception");
                attempts++;
            }
            catch(NoSuchElementException e) {
                logWithScreenshot(null, e.getMessage(), "FAIL");
                logInfo(null,"<b>Calling Function: </b>"+Thread.currentThread().getStackTrace()[2].getMethodName());
                logInfo(null,"<b>Calling Class: </b>"+Thread.currentThread().getStackTrace()[2].getClassName()+ " ("+Thread.currentThread().getStackTrace()[2].getLineNumber()+")");
                Assert.fail(e.getMessage());
                break;
            }
        }
        return obj;
    }




    public void logStringEqual(WebElement obj,String st1,String st2){
        if (st1.equals(st2))
            logPass(obj,"<b>VERIFICATION:</b> String Comparison Passed <br />" +"<b>Value: </b>" + st2);
        else
            logFail(obj,"<b>VERIFICATION:</b> String Comparison Failed <br />" +"<b>EXPECTED: </b>" + st2+"<br /><b>ACTUAL:</b> " + st1);

    }
    public void logInfo(WebElement Obj,String description){

        //GV.testSubStep=GV.test;
        //GV.test=GV.test.createNode(description);

        if (GV.logLevel.contains("2")) {     //Save image of each Passing Object as well
            logWithScreenshot(Obj,description,"INFO");
        }

        else{     //Dont Save image of each Passing Object as well
            GV.test.log(Status.INFO,description);
            GV.logger.info(GV.suiteName+" | "+"ACTION: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
        }

        //GV.test=GV.testSubStep;


    }

    public void logPass(WebElement Obj,String description) {

        //GV.testSubStep=GV.test;
        //GV.test=GV.test.createNode(description);

        if (GV.logLevel.contains("1")) {     //Save image of each Passing Object as well
            logWithScreenshot(Obj,description,"PASS");
        }

       else{     //Dont Save image of each Passing Object as well
            GV.test.pass(description);
            GV.logger.info(GV.suiteName+" | "+"PASS: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
        }

        //GV.test=GV.testSubStep;

    }

    public void logFail(WebElement Obj,String description){

        //GV.testSubStep=GV.test;
        //GV.test=GV.test.createNode(description);

        if (GV.logLevel.contains("0")) {     //Save image of each Passing Object as well
            logWithScreenshot(Obj, description, "FAIL");
        }
        else{
            GV.test.log(Status.FAIL, description);
            GV.logger.info(GV.suiteName+" | "+"FAIL: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
        }

        //GV.test=GV.testSubStep;


    }

    public void logWarning(WebElement Obj,String description){

        //GV.testSubStep=GV.test;
        //GV.test=GV.test.createNode(description);

        if (GV.logLevel.contains("0")) {     //Save image of each Passing Object as well
            logWithScreenshot(Obj, description, "WARNING");
        }
        else{
            GV.test.log(Status.WARNING, description);
            GV.logger.info(GV.suiteName+" | "+"WARNING: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
        }

        //GV.test=GV.testSubStep;


    }

    public void logError(ITestResult iTestResult, String description) {

        //GV.testSubStep=GV.test;
        //GV.test=GV.test.createNode(description);
        if (iTestResult != null)
        {
            logWithScreenshot(null, iTestResult.getThrowable().getCause().toString(), "ERROR");

        //GV.test.log(Status.ERROR, iTestResult.getThrowable().getCause().toString());
        //GV.logger.error(iTestResult.getThrowable().getCause().toString());
            StackTraceElement[] stackTrace = iTestResult.getThrowable().getCause().getStackTrace();
        for (int i = 0; i < stackTrace.length; i++)
            GV.test.log(Status.INFO, iTestResult.getThrowable().getCause().getStackTrace()[i].toString());
        }
        else
            logWithScreenshot(null, description, "ERROR");

        //GV.test=GV.testSubStep;
    }

    public void logWithScreenshot(WebElement Obj,String description,String result){
        File source=null;
        if (Obj!=null){
            if (GV.driver instanceof JavascriptExecutor) {
                ((JavascriptExecutor)GV.driver).executeScript("arguments[0].style.border='3px solid red'", Obj);
            }
        }


        try{
            WebDriver augmentedDriver = new Augmenter().augment(GV.driver);
            source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
        }
        catch (Exception e){
            GV.test.log(Status.WARNING,"Cannot initialize Augemented Driver for Chrome Driver");
            GV.logger.warn("Cannot copy Raw screenshot to directory");
        }


        if (Obj!=null){
            if (GV.driver instanceof JavascriptExecutor) {
                ((JavascriptExecutor)GV.driver).executeScript("arguments[0].style.border=''", Obj);
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSS").format(Calendar.getInstance().getTime());
        String filePath=System.getProperty("user.dir")+ "\\test-output\\extentReports\\" + timeStamp + ".png";

        //Call getScreenshotAs method to create image file

        try{
            if (source!=null)
                FileUtils.copyFile(source, new File(filePath));
        }
        catch (Exception e){
            GV.test.log(Status.WARNING,"Cannot copy Raw screenshot to directory");
            GV.logger.warn("Cannot copy Raw screenshot to directory");
        }

        try {
            if (result.compareTo("PASS")==0) {
                //GV.test.log(Status.PASS, description, MediaEntityBuilder.createScreenCaptureFromPath(filePath).build());
                GV.test.log(Status.PASS, description, MediaEntityBuilder.createScreenCaptureFromPath(timeStamp + ".png").build());
                GV.logger.info("PASS: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
            }
            if (result.compareTo("FAIL")==0) {
                GV.test.log(Status.FAIL, description, MediaEntityBuilder.createScreenCaptureFromPath(timeStamp + ".png").build());
                GV.logger.info("FAIL: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
            }
            if (result.compareTo("INFO")==0) {
                GV.test.log(Status.INFO, description, MediaEntityBuilder.createScreenCaptureFromPath(timeStamp + ".png").build());
                GV.logger.info("ACTION: " + description.replace("<b>", "").replace("</b>", "").replace("<br />", "\r\n"));
            }
            if (result.compareTo("ERROR")==0) {
                GV.test.log(Status.ERROR, description, MediaEntityBuilder.createScreenCaptureFromPath(timeStamp + ".png").build());
                GV.logger.error("ERROR: " + description);
            }
            if (result.compareTo("WARNING")==0) {
                GV.test.log(Status.WARNING, description, MediaEntityBuilder.createScreenCaptureFromPath(timeStamp + ".png").build());
                GV.logger.error("WARNING: " + description);
            }
        }
        catch (Exception e){
            GV.test.log(Status.ERROR,e.getCause().toString());
            GV.logger.error(e.getCause().toString());
        }
    }


    public String[] coordinatesFetchImage(WebElement obj){
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        String result[]={"",""};
        result[0]= (String) jse.executeScript("return arguments[0].getClientRects()[0].x.toString()", obj);
        result[1]= (String) jse.executeScript("return arguments[0].getClientRects()[0].y.toString()", obj);
        return result;
    }

    public String coordinatesFetch(WebElement obj,String selection){
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        return (String) jse.executeScript("return arguments[0].getClientRects()[0]."+selection+".toString()", obj);
    }

    public String javascriptExecutor(WebElement obj,String query){
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        return jse.executeScript(query, obj).toString();
    }

    public void javascriptExecutorNoReturn(WebElement obj,String query){
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        jse.executeScript(query, obj);
    }

    public String javascriptExecutor(String query){
        String result;
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        Object result2 = jse.executeScript(query);
        return (String) result2;
        //result= jse.executeScript(query).toString();
        //return result;
    }



    public int findIndexInArray(String[][] resultSet,int column,String value){
        for (int i=0;i<resultSet.length;i++)
        {
            if (resultSet[i][column]!=null && resultSet[i][column].equals(value)){
                return i;
            }
        }
        return -1;
    }

    public int containsIndexInArray(String[][] resultSet,int column,String value){
        for (int i=0;i<resultSet.length;i++)
        {
            if (resultSet[i][column]!=null && resultSet[i][column].contains(value)){
                return i;
            }
        }
        return -1;
    }

    public String[][] joinArrays(String[][] array1,String[][] array2){
        String[][] completeData=new String[array1.length+array2.length][10];
        int iterator=0,i;
        for (i=0;i<array1.length;i++){
            completeData[iterator]=array1[i];
            iterator++;
        }
        for (i=0;i<array2.length;i++){
            completeData[iterator]=array2[i];
            iterator++;
        }
        return completeData;

    }

    public String[][] sortArrays(String[][] completeData){
        String[][] sortedData=new String[1][10];
        for (int i=0;i<completeData.length;i++){
            sortedData=positionArrays(sortedData,completeData[i]);
        }
        return sortedData;

    }

    public int findInArray(String[][] completeData,int index,String Value){
        for (int i=0;i<completeData.length;i++){
            if (completeData[i][index].equals(Value)){
                return i;
            }
        }
        return -1;

    }

    private String[][] positionArrays(String[][] completeData,String[] newEntry){
        Boolean foundFlag=false;
        int i=0;
        String[] temp1=new String[10];
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, d MMM yyyy", Locale.ENGLISH);
        String[][] newData=new String[completeData.length+1][10];
        for (i=0;i<completeData.length;i++){
            if (!foundFlag && completeData[i][0]==null){
                foundFlag=true;
                temp1=completeData[i];
                newData[i]=newEntry;

            }
            else if (!foundFlag && Integer.valueOf(newEntry[2].replace(" Days on Market",""))<=Integer.valueOf(completeData[i][2].replace(" Days on Market",""))){
                temp1=completeData[i];
                //completeData[i]=newEntry;
                foundFlag=true;
                newData[i]=newEntry;

            }
            else if(foundFlag){
                newData[i]=temp1;
                temp1=completeData[i];

            }
            else{
                newData[i]=completeData[i];
            }
        }
        newData[i]=temp1;
        return newData;

    }


    public boolean compareImagesNoLog(WebElement obj, String path) {
        JavascriptExecutor jse = (JavascriptExecutor)GV.driver;
        jse.executeScript("arguments[0].scrollIntoView()", obj);
        String coordinates[]={"",""};
        coordinates[0]= GV.CF.coordinatesFetch(obj,"x");
        coordinates[1]= GV.CF.coordinatesFetch(obj,"y");
        BufferedImage expectedImage=null;
        try {
            expectedImage = ImageIO.read(new File(System.getProperty("user.dir") + path));
        }
        catch (Exception e){
            System.out.println(e.getMessage());

        }

        // Get entire page screenshot
        File screenshot = ((TakesScreenshot)GV.driver).getScreenshotAs(OutputType.FILE);
        BufferedImage  fullImg=null;
        try{fullImg = ImageIO.read(screenshot);}
        catch(Exception e){
            GV.CF.logInfo(null,"Exception - Trying to read screenshot, CommonFunc.Java|compareImagesNoLog");
        }

        // Get the location of element on the page
        Point point=new Point(0,0);
        point.x=(int)Math.round(Double.valueOf(coordinates[0]));
        point.y=(int)Math.round(Double.valueOf(coordinates[1]));

        // Get width and height of the element
        int eleWidth = obj.getSize().getWidth();
        int eleHeight = obj.getSize().getHeight();

        // Crop the entire page screenshot to get only element screenshot
        BufferedImage actualImage=null;
        if (fullImg!=null)
            actualImage= fullImg.getSubimage(point.getX(), point.getY(),eleWidth, eleHeight);
        //Screenshot logoImageScreenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(GV.driver, obj);
        //BufferedImage actualImage = logoImageScreenshot.getImage();

        ImageDiffer imgDiff = new ImageDiffer();
        ImageDiff diff=null;
        if (actualImage!=null && expectedImage!=null)
            diff= imgDiff.makeDiff(actualImage, expectedImage);
        if (diff!=null && diff.withDiffSizeTrigger(5000).hasDiff()){
            return false;
        }
        else{
            try{

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss ");
                Date date = new Date();
                String date1= dateFormat.format(date);
                String imagePath=System.getProperty("user.dir")+"\\test-output\\" + date1  + "_1.png";
                if (diff!=null)
                    ImageIO.write(diff.getMarkedImage(),"png",new File(imagePath));
                GV.test.addScreenCaptureFromPath(imagePath);
                imagePath=System.getProperty("user.dir")+"\\test-output\\" + date1  + "_2.png";
                if (expectedImage!=null)
                    ImageIO.write(expectedImage,"png",new File(imagePath));
                GV.test.addScreenCaptureFromPath(imagePath);
                imagePath=System.getProperty("user.dir")+"\\test-output\\" + date1  + "_3.png";
                if (actualImage!=null)
                    ImageIO.write(actualImage,"png",new File(imagePath));
                GV.test.addScreenCaptureFromPath(imagePath);
                return true;
            }
            catch (Exception e){
                GV.CF.logInfo(null,"Exception - Trying to write screenshot of error, CommonFunc.Java|compareImagesNoLog");
            }
        }
        return true;

    }

    public boolean isAlertPresent() {
        try {
            GV.driver.switchTo().alert();
            return true;
        }// try
        catch (Exception e) {
            return false;
        }// catch
    }

    public void delay(int millisec){

        GV.logger.info("DELAY: " + "Delaying for " + Double.valueOf(millisec)/1000 + " seconds");
        try{
            Thread.sleep(millisec);
        }
        catch (Exception e){
            GV.CF.logInfo(null,"Exception - Trying to implement sleep request, CommonFunc.Java|isAlertPresent");
        }

    }

    public WebElement getElement(By elementLocator,String data){
        if (data!=null && !data.equals("!SKIP")){
            return GV.CF.findElement(elementLocator);
        }
        else {
            GV.CF.logInfo(null, "Skipping Object: " + elementLocator.toString());
            return null;

        }
    }

    public void clearCookieBanner(){
        Action Ac=new Action(GV);
        if (GV.testData.get("cookieRemoved")==null) {
            try {
                Ac.click(GV.CF.findElement(cookieBanner));
            } catch (Exception e) {
                System.out.println("Exception - Trying to find cookie Banner, CommonFunc.Java|clearCookieBanner");
            }
            GV.testData.put("cookieRemoved", "true");
            GV.CF.logInfo(null, "Cookie Banner Removed");
        }
    }

    public void logSkip(String description){
        GV.test.log(Status.SKIP,description);
        GV.skipFlag=true;
        //throw new SkipException(description);

    }

    public void logStep(int StepNumber,String Description){

        //Dont Save image of each Passing Object as well
        GV.test=GV.testTest.createNode("<b>Step# "+StepNumber+":</b> "+Description);

        //GV.test.log(Status.INFO,"<p style=\"color: #22A1C4;\">------"+"<b>Step# "+StepNumber+":</b> "+Description+"-----------</p>");
        GV.logger.info(GV.suiteName+" | "+"Step: " + StepNumber);


    }

    public void logStep(String Description){

        //Dont Save image of each Passing Object as well
        GV.test=GV.testTest.createNode(Description);

        //GV.test.log(Status.INFO,"<p style=\"color: #22A1C4;\">------"+"<b>Step# "+StepNumber+":</b> "+Description+"-----------</p>");
        GV.logger.info(GV.suiteName+" | "+"Step: " + Description);


    }



}
